<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$email = $_POST['email'];
$chooseTheRightGuards = $_POST['chooseTheRightGuards'];
$address = $_POST['address'];
$totalGutterLength = $_POST['totalGutterLength'];
$treeCoverage = $_POST['treeCoverage'];
$pineSelectedOption = $_POST['pineSelectedOption'];
$roofOption = $_POST['roofOption'];
$yardSelectedOption = $_POST['yardSelectedOption'];
$gutterSize = $_POST['gutterSize'];

$servername = "localhost";
$username = "root";
$password = "";
$dbName = "atlas";

try {
  $dbh = new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
$query = "INSERT INTO `atlas_gutter`(`email`, `guard`, `address`, `totalLength`, `treeCoverage`, `yardOptions`, `roofOption`, `yardPets`, `gutterSize`) VALUES ('$email','$chooseTheRightGuards','$address','$totalGutterLength','$treeCoverage','$pineSelectedOption','$roofOption','$yardSelectedOption','$gutterSize')";
$insertDetails = $dbh->prepare($query);

if($insertDetails->execute()){
    require 'vendor/autoload.php';
    $mail = new PHPMailer(true);
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [220, 320]]);
    $mpdf->WriteHTML('<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Result</title>
        <style>
            *{
                margin: 0;
                padding: 0;
            }
            body{font-family: "Avenir";}
    
            @font-face {
                font-family: "Avenir";
                src: url(font/AvenirLTStd-Black.otf);
                src: url(font/AvenirLTStd-Book.otf);
                src: url(font/AvenirLTStd-Roman.otf);
            }
            .congo_container
            {
                position: relative;
                background-color: #FFF;
                background: url(images/bg_trans.png)repeat;
            }
            .blue_eclipse
            {
                background: url(images/top_eclipse2.png) no-repeat;
                background-position: top;
                  background-size: 100%; 
                margin: 0 auto;
                background-color: #FFF;
                text-align: center;
            }
            .blue_eclipse span
            {
                font-size: 16px;
                color: #FFF;
                line-height: normal;
                font-weight: bold;
                display: block;
            }
            .blue_eclipse p
            {
                font-size: 15px;
                color: #FFF;
                line-height: normal;
                text-transform: capitalize;
            }
            .top_block
            {
                padding: 75px 0;
                width: 80%;
                margin: 0 auto;
                text-align: center;
            }
            .top_block img
            {
                margin: 15px 0;
            }
            .white_box
            {
                width: 80%;
                margin:-60px auto 0;
                text-align: center;
                background-color: #FFF;
                z-index: 1;
                border-radius:20px; box-shadow:0px 0px 20px 0px #707070;
            }
            .white_box span
            {
                font-size: 12px;
                color: #7b7b7b;
                font-style: italic;
                font-weight: lighter;
                padding: 15px 0 10px;
                display: block;
            }
            .white_box h4
            {
                font-size: 20px;
                color: #7b7b7b;
                font-style: normal;
                font-weight:bold;
                margin: 0 0 30px 0;
            }
            .white_box h3
            {
                color: #0b2870;
                font-size: 32px;
                font-weight: bold;
            }
            .white_box h5
            {
                color: #0b2870;
                font-size: 20px;
                font-weight: bold;
                margin: 0 0 30px 0;
            }
            .white_box p
            {
                color: #0b2870;
                line-height: normal;
                font-weight: lighter;
                width: 80%;
                font-size: 16px;
                margin: 0 auto;
            }
            .sold span
            {
                color: #024990;
            }
            .sold
            {
                margin: 15px 0 30px 0;
                text-align: center;
            }
            .sold span img {
                top: 8px;left: 8px;
            }
            @media only screen and (max-width:450px){
      .blue_box{margin:5% 2.5% 5% 7.5%;}
      .sky_blue_box{margin:5% 7.5% 5% 2.5%;}
      .sky_blue_box2 img{left: 10px;}
    }
        </style>
    </head>
    <body class="congo_container">
        <div class="blue_eclipse">
            <div class="top_block">
                <span>Congraulations,</span><br>
                <span>Gutter Guard Guru!</span><br>
                <img src="images/email_logo.png">
                <p>is the perfect gutter guard solution for your home</p>
            </div>
        </div>
        <div class="white_box">
            <span>Based on your home"s perimeter and gutter size</span>
            <div style="width: 100%;padding: 40px; text-align: left;">
                <h4 id="selectedOption">Here is your Result For : '.$chooseTheRightGuards.'</h4>
                <h4 id="address">Your Address : '.$address.'</h4>
                <h4 id="totalGutterLength">Your Total Gutter Length : '.$totalGutterLength.'</h4>
                <h4 id="treeCoverage">Tree Coverage : '.$treeCoverage.'</h4>
                <h4 id="pineSelectedOption">Yard Options : '.$pineSelectedOption.'</h4>
                <h4 id="roofOption">Your Roof : '.$roofOption.'</h4>
                <h4 id="yardPets">Yard Pets : '.$yardSelectedOption.'</h4>
                <h4 id="gutterSizeData">Gutter Size : '.$gutterSize.'</h4>
            </div>
            <h5>of Atlas Gutter Guard</h5>
            <p style="padding-bottom: 20px;">Our 6" gutter guards are not availble in stores yet.Shop online now to get started today! </p>
        </div>
        <div class="sold">
            <span>Sold at</span>
            <span><img src="images/loweslogo_blue.png"></span>
        </div>
     </body>
    </html>');
    $mpdf->Output('uploads/result.pdf','f');
    try {
        $mail->SMTPDebug = 0;                                       
        $mail->isSMTP();                                            
        $mail->Host       = 'smtp.gmail.com;';                    
        $mail->SMTPAuth   = true;                             
        $mail->Username   = 'testingphpm@gmail.com';                 
        $mail->Password   = 'Nirav@@123';                        
        $mail->SMTPSecure = 'tls';                              
        $mail->Port       = 587;  
        $mail->setFrom('testingphpm@gmail.com');           
        $mail->addAddress($email);
        $mail->isHTML(true);                                  
        $mail->Subject = 'Result From Atlas Gutter Guru';
        $mail->Body    = 'Dear Sir/Madam, Thankyou for your valuable Time. Please find attached Result';
        $mail->addAttachment('uploads/result.pdf');
        $mail->send();
        if(unlink('uploads/result.pdf')){
            echo 'success';
        }else{
            echo "failed";
        }
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
}else{
    echo 'failed';
}

  
?>