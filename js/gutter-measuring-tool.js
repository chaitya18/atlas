let createAgain = false;
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("GutterMeasuringTool", [], factory);
	else if(typeof exports === 'object')
		exports["GutterMeasuringTool"] = factory();
	else
		root["GutterMeasuringTool"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 13);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),
/* 4 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
var stylesInDom = {},
	memoize = function(fn) {
		var memo;
		return function () {
			if (typeof memo === "undefined") memo = fn.apply(this, arguments);
			return memo;
		};
	},
	isOldIE = memoize(function() {
		return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
	}),
	getHeadElement = memoize(function () {
		return document.head || document.getElementsByTagName("head")[0];
	}),
	singletonElement = null,
	singletonCounter = 0,
	styleElementsInsertedAtTop = [];

module.exports = function(list, options) {
	if(typeof DEBUG !== "undefined" && DEBUG) {
		if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};
	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (typeof options.singleton === "undefined") options.singleton = isOldIE();

	// By default, add <style> tags to the bottom of <head>.
	if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

	var styles = listToStyles(list);
	addStylesToDom(styles, options);

	return function update(newList) {
		var mayRemove = [];
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			domStyle.refs--;
			mayRemove.push(domStyle);
		}
		if(newList) {
			var newStyles = listToStyles(newList);
			addStylesToDom(newStyles, options);
		}
		for(var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];
			if(domStyle.refs === 0) {
				for(var j = 0; j < domStyle.parts.length; j++)
					domStyle.parts[j]();
				delete stylesInDom[domStyle.id];
			}
		}
	};
}

function addStylesToDom(styles, options) {
	for(var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];
		if(domStyle) {
			domStyle.refs++;
			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}
			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];
			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}
			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles(list) {
	var styles = [];
	var newStyles = {};
	for(var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};
		if(!newStyles[id])
			styles.push(newStyles[id] = {id: id, parts: [part]});
		else
			newStyles[id].parts.push(part);
	}
	return styles;
}

function insertStyleElement(options, styleElement) {
	var head = getHeadElement();
	var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
	if (options.insertAt === "top") {
		if(!lastStyleElementInsertedAtTop) {
			head.insertBefore(styleElement, head.firstChild);
		} else if(lastStyleElementInsertedAtTop.nextSibling) {
			head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			head.appendChild(styleElement);
		}
		styleElementsInsertedAtTop.push(styleElement);
	} else if (options.insertAt === "bottom") {
		head.appendChild(styleElement);
	} else {
		throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
	}
}

function removeStyleElement(styleElement) {
	styleElement.parentNode.removeChild(styleElement);
	var idx = styleElementsInsertedAtTop.indexOf(styleElement);
	if(idx >= 0) {
		styleElementsInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement(options) {
	var styleElement = document.createElement("style");
	styleElement.type = "text/css";
	insertStyleElement(options, styleElement);
	return styleElement;
}

function createLinkElement(options) {
	var linkElement = document.createElement("link");
	linkElement.rel = "stylesheet";
	insertStyleElement(options, linkElement);
	return linkElement;
}

function addStyle(obj, options) {
	var styleElement, update, remove;

	if (options.singleton) {
		var styleIndex = singletonCounter++;
		styleElement = singletonElement || (singletonElement = createStyleElement(options));
		update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
		remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
	} else if(obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function") {
		styleElement = createLinkElement(options);
		update = updateLink.bind(null, styleElement);
		remove = function() {
			removeStyleElement(styleElement);
			if(styleElement.href)
				URL.revokeObjectURL(styleElement.href);
		};
	} else {
		styleElement = createStyleElement(options);
		update = applyToTag.bind(null, styleElement);
		remove = function() {
			removeStyleElement(styleElement);
		};
	}

	update(obj);

	return function updateStyle(newObj) {
		if(newObj) {
			if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
				return;
			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;
		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag(styleElement, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (styleElement.styleSheet) {
		styleElement.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = styleElement.childNodes;
		if (childNodes[index]) styleElement.removeChild(childNodes[index]);
		if (childNodes.length) {
			styleElement.insertBefore(cssNode, childNodes[index]);
		} else {
			styleElement.appendChild(cssNode);
		}
	}
}

function applyToTag(styleElement, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		styleElement.setAttribute("media", media)
	}

	if(styleElement.styleSheet) {
		styleElement.styleSheet.cssText = css;
	} else {
		while(styleElement.firstChild) {
			styleElement.removeChild(styleElement.firstChild);
		}
		styleElement.appendChild(document.createTextNode(css));
	}
}

function updateLink(linkElement, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	if(sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = linkElement.href;

	linkElement.href = URL.createObjectURL(blob);

	if(oldSrc)
		URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(__webpack_require__(10));

var _asyncToGenerator2 = _interopRequireDefault(__webpack_require__(9));

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

var _loading = _interopRequireDefault(__webpack_require__(14));

var _toast = _interopRequireDefault(__webpack_require__(18));

var GeoJsonTool = /*#__PURE__*/function () {
  function GeoJsonTool(_ref) {
    var parentContainer = _ref.parentContainer,
        measure = _ref.measure,
        mapTool = _ref.mapTool,
        map = _ref.map;
    (0, _classCallCheck2["default"])(this, GeoJsonTool);
    this._measure = measure;
    this._mapTool = mapTool;
    this._parentContainer = parentContainer;
    this._map = map;
  }

  (0, _createClass2["default"])(GeoJsonTool, [{
    key: "setPolygon",
    value: function setPolygon(latLng) {
      var _this = this;

      var self = this;
      this._loading = new _loading["default"](this._parentContainer);

      this._loading.show();

      this._measure.reset();

      this._mapTool.setLocation(latLng, function (zoom) {
        try {
          _this._getGeojson(latLng).then(function (response) {
            if (response && response.length) {
              self._map.setZoom(zoom - 2);

              self._setMeasure(response);
            } else {
              _this._getAutoPolygon(latLng, zoom).then(function (response) {
                if (response && response.length) {
                  self._setMeasure(response);
                } else {
                  self._setMeasure([]);
                }
              });
            }
          });
        } catch (e) {
          console.warn(e);
          var toast = new _toast["default"]();
          toast.show({
            text: "Oops, Something is wrong",
            type: 'error'
          });
        }
      });
    }
  }, {
    key: "_setMeasure",
    value: function _setMeasure(coords) {
      var _this2 = this;

      var self = this;

      this._measure.startMeasure({
        points: coords
      });

      this._sleep(100).then(function () {
        var showEditor = !coords.length ? true : false;

        self._measure.endMeasure(showEditor);

        _this2._loading.hide();
      });
    }
  }, {
    key: "_getGeojson",
    value: function () {
      var _getGeojson2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(latLng) {
        var coords, osmResponse, osmData, geojson, coordinates;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                coords = [];
                _context.prev = 1;
                _context.next = 4;
                return fetch("https://nominatim.openstreetmap.org/search.php?q=".concat(latLng.lat() + ',' + latLng.lng(), "&polygon_geojson=1&format=jsonv2&limit=1"));

              case 4:
                osmResponse = _context.sent;
                _context.next = 7;
                return osmResponse.json();

              case 7:
                osmData = _context.sent;
                geojson = osmData && osmData.length ? osmData[0].geojson : undefined;

                if (geojson && geojson.type == "Polygon" && geojson.coordinates && geojson.coordinates.length) {
                  coordinates = geojson.coordinates[0];
                  coordinates.forEach(function (coord) {
                    coords.push({
                      lat: coord[1],
                      lng: coord[0]
                    });
                  });
                }

                _context.next = 15;
                break;

              case 12:
                _context.prev = 12;
                _context.t0 = _context["catch"](1);
                console.warn(_context.t0);

              case 15:
                return _context.abrupt("return", coords);

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 12]]);
      }));

      function _getGeojson(_x) {
        return _getGeojson2.apply(this, arguments);
      }

      return _getGeojson;
    }()
  }, {
    key: "_getAutoPolygon",
    value: function () {
      var _getAutoPolygon2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(latLng, zoom) {
        var convertedPointsMain, map, apResponse, apData, i, conv_point;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                convertedPointsMain = [];
                _context2.prev = 1;
                map = document.getElementById("map");

                if (!map) {
                  _context2.next = 11;
                  break;
                }
                if(sessionStorage.getItem('latitude')!= "" && sessionStorage.getItem('longitude')!=""){
                  sessionStorage.setItem('latitude',"");
                  sessionStorage.setItem('longitude',"");
                }
                sessionStorage.setItem('latitude',latLng.lat());
                sessionStorage.setItem('longitude',latLng.lng());
                _context2.next = 6;
                return fetch("https://autopolygon-house-measuretool.herokuapp.com/get?lat=".concat(latLng.lat(), "&lng=").concat(latLng.lng(), "&zoom=").concat(zoom, "&width=").concat(map.offsetWidth, "&height=").concat(map.offsetHeight));

              case 6:
                apResponse = _context2.sent;
                _context2.next = 9;
                return apResponse.json();

              case 9:
                apData = _context2.sent;

                if (apData && apData.length) {
                  for (i = 0; i < apData.length; i++) {
                    conv_point = {
                      x: Math.round(apData[i][1]),
                      y: Math.round(apData[i][0])
                    };
                    convertedPointsMain[i] = this._pointToLatLng(conv_point);
                  }
                }

              case 11:
                _context2.next = 16;
                break;

              case 13:
                _context2.prev = 13;
                _context2.t0 = _context2["catch"](1);
                console.warn(_context2.t0);

              case 16:
                return _context2.abrupt("return", this._getCoords(convertedPointsMain));

              case 17:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[1, 13]]);
      }));

      function _getAutoPolygon(_x2, _x3) {
        return _getAutoPolygon2.apply(this, arguments);
      }

      return _getAutoPolygon;
    }()
  }, {
    key: "_pointToLatLng",
    value: function _pointToLatLng(point) {
      if (this._map) {
        var topRight = this._map.getProjection().fromLatLngToPoint(this._map.getBounds().getNorthEast());

        var bottomLeft = this._map.getProjection().fromLatLngToPoint(this._map.getBounds().getSouthWest());

        var scale = Math.pow(2, this._map.getZoom());
        var worldPoint = new google.maps.Point(point.x / scale + bottomLeft.x, point.y / scale + topRight.y);
        return this._map.getProjection().fromPointToLatLng(worldPoint);
      }

      return null;
    }
  }, {
    key: "_getPolygon",
    value: function _getPolygon(_ref2) {
      var _this3 = this;

      var point = _ref2.point,
          zoom = _ref2.zoom;

      // get the world coordinate for the center point
      var centWCord = this._map.getProjection().fromLatLngToPoint(point); //returns x,y(point) - world coordinate
      //convert to pixel coordinate


      var centPix = new google.maps.Point(centWCord.x * Math.pow(2, zoom), centWCord.y * Math.pow(2, zoom)); //create 4 boundary points

      var boundaryPoints = [];
      boundaryPoints.push(this._getBoundaryPoint(centPix, 80, 80));
      boundaryPoints.push(this._getBoundaryPoint(centPix, -80, 80));
      boundaryPoints.push(this._getBoundaryPoint(centPix, -80, -80));
      boundaryPoints.push(this._getBoundaryPoint(centPix, 80, -80));
      boundaryPoints.push(this._getBoundaryPoint(centPix, 80, 80)); //convert back to world coordinates

      var worldCoords = [];
      worldCoords = boundaryPoints.map(function (boundaryPoint) {
        return _this3._getWorldCoordinates(boundaryPoint, zoom);
      });
      var coords = [];
      coords = worldCoords.map(function (worldCoord) {
        return _this3._getLatLngCoordinates(worldCoord);
      });
      return this._getCoords(coords);
    }
  }, {
    key: "_getBoundaryPoint",
    value: function _getBoundaryPoint(point, x, y) {
      return new google.maps.Point(point.x + x, point.y + y);
    }
  }, {
    key: "_getWorldCoordinates",
    value: function _getWorldCoordinates(point, zoom) {
      point.x = point.x / Math.pow(2, zoom);
      point.y = point.y / Math.pow(2, zoom);
      return point;
    }
  }, {
    key: "_getLatLngCoordinates",
    value: function _getLatLngCoordinates(point) {
      return this._map.getProjection().fromPointToLatLng(point);
    }
  }, {
    key: "_sleep",
    value: function _sleep(time) {
      return new Promise(function (resolve) {
        return setTimeout(resolve, time);
      });
    }
  }, {
    key: "_getCoords",
    value: function _getCoords(coords) {
      var points = [];
      coords.forEach(function (p) {
        if (p) {
          points.push({
            lat: p.lat(),
            lng: p.lng()
          });
        }
      });
      return points;
    }
  }]);
  return GeoJsonTool;
}();

exports["default"] = GeoJsonTool;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

var MapTool = /*#__PURE__*/function () {
  function MapTool(_ref) {
    var parentContainer = _ref.parentContainer;
    (0, _classCallCheck2["default"])(this, MapTool);
    this.parentContainer = parentContainer;
    this._zoom = 21;
  }

  (0, _createClass2["default"])(MapTool, [{
    key: "load",
    value: function load() {
      var mapContainer = document.createElement("div");
      mapContainer.id = "map";
      if (this.parentContainer) this.parentContainer.appendChild(mapContainer);
      this.map = new google.maps.Map(document.getElementById('map'), {
        zoom: this._zoom,
        center: {
          lat: 38.7746891,
          lng: -121.2924987
        },
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        tilt: 0,
        zoomControl: false,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
        }
      });
      return this.map;
    }
  }, {
    key: "setLocation",
    value: function setLocation(latLng, cb) {
      var maxZoomService = new google.maps.MaxZoomService();
      var self = this;
      maxZoomService.getMaxZoomAtLatLng(latLng, function (result) {
        var zoom = self._zoom;

        if (result.status === "OK") {
//          zoom = result.zoom;
	    console.log(zoom);
        }

        self.map.setCenter(latLng);
        self.map.setZoom(zoom);

        if (cb) {
          cb(zoom);
        }
      });
    }
  }]);
  return MapTool;
}();

exports["default"] = MapTool;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

var _measuretoolGooglemapsV = _interopRequireDefault(__webpack_require__(28));

var _toolActions = _interopRequireDefault(__webpack_require__(19));

var _modal = _interopRequireDefault(__webpack_require__(15));

var _notification = _interopRequireDefault(__webpack_require__(16));

var _header = _interopRequireDefault(__webpack_require__(12));

var _contextMenu = _interopRequireDefault(__webpack_require__(11));

var _popup = _interopRequireDefault(__webpack_require__(17));

var Measure = /*#__PURE__*/function () {
  function Measure(_ref) {
    var parentContainer = _ref.parentContainer,
        map = _ref.map,
        el = _ref.el;
    (0, _classCallCheck2["default"])(this, Measure);
    this.parentContainer = parentContainer;
    this.map = map;
    this._el = el;
    this._measureTool = null;
    this._currentMeasureCancelled = false;
    this._autoPolygonNotFound = false;
    this._updatePolyLineMode = false;
    this.polyLine = null;
    this.polyLines = [];
    this.results = [];
    this.toolActions = new _toolActions["default"](this.parentContainer);
    this.notification = new _notification["default"](this.parentContainer);
    this.header = new _header["default"](this.parentContainer);
    this.mainPage = false;
  }

  (0, _createClass2["default"])(Measure, [{
    key: "load",
    value: function load() {
      var _this = this;

      if (this.map) {
        this._measureTool = new _measuretoolGooglemapsV["default"](this.map, {
          contextMenu: false,
          unit: _measuretoolGooglemapsV["default"].UnitTypeId.IMPERIAL,
          tooltip: false
        });
      }

      var measureButtonsContainer = document.createElement("div");
      measureButtonsContainer.id = "gmt-measure-buttons-container";
      measureButtonsContainer.innerHTML = "\n            <div id=\"gmt-cancel-measure-button-container\" style=\"width: 50%;\">\n        <button id=\"gmt-cancel-measure-button\" class=\"plus_sign\" style='background:transparent;border:none;right:0;left:0;margin:0 auto;'>\n<img src=\"images/Group 208.png\" style='position:relative;top:1px;'>\n</button>\n</div>\n\n<div id=\"gmt-end-measure-button-container\" class=\"next_sign \" style=\"width: 50%;\">\n<button id=\"gmt-end-measure-button\" style=\"background: transparent; position: absolute;right: 0;border: none;\">\n<img src=\"images/Group 213.png\">\n</button>\n</div>\n";
      measureButtonsContainer.style.visibility = "hidden";
      this.parentContainer.appendChild(measureButtonsContainer);
      var cancelButton = document.getElementById("gmt-cancel-measure-button");
      var endButton = document.getElementById("gmt-end-measure-button");

      if (cancelButton) {
        cancelButton.addEventListener("click", function () {
          _this.mainPage = false;
          _this._showToolActions("measuring");
          document.getElementById('create_gutter_section').style.display = "";
          _this.cancel();
        });
      }

      if (endButton) {
        endButton.addEventListener("click", function () {
          _this.mainPage = false;
          if (_this._measureTool) _this._measureTool.end();

          _this._showNotification('step5');

          _this._showHeader("edit");
          document.getElementById('create_btn_new').style.display = '';
          document.getElementById('top_corner2').style.display = 'none';
          document.getElementById('top_corner3').style.display = 'none';
          document.getElementById('next-step').style.display = '';
          document.getElementById('gmt-tool-actions-result').style.display='';
          _this.map.setOptions({
            zoomControl: false
          });
        });
      }

      if (this._measureTool) {
        this._measureTool.addListener("measure_end", function (e) {
          _this.onEndMeasure(e, _this);
        });
      }

      if (this._measureTool) {
        this._measureTool.addListener("measure_change", function (e) {
          _this.onChangeMeasure(e, _this);
        });
      }
    }
  }, {
    key: "saveMeasure",
    value: function saveMeasure() {
      this.mainPage = true;
      this.toolActions.hide();

      this._defineInitialFlow();

      this.header.hide();
      var mainContainer = document.getElementById(this._el);

      if (mainContainer && mainContainer.parentElement && this._parentBottom) {
        mainContainer.parentElement.style.bottom = this._parentBottom;
      }

      if (this._contextMenu) this._contextMenu.hide();
    }
  }, {
    key: "startMeasure",
    value: function startMeasure(_ref2) {
      var points = _ref2.points;
      this.toolActions.hide();
      this.show();

      if (this._measureTool) {
        this._measureTool.start(points || []);

        if (!points || points.length < 2) {
          var cancelButtonConatiner = document.getElementById("gmt-cancel-measure-button-container");
          var topCorner = document.getElementById("top_corner");
          if (cancelButtonConatiner) 
          	{//cancelButtonConatiner.style.width = "100%";
          cancelButtonConatiner.style.width = "0%";
          cancelButtonConatiner.style.display = "none";
        }
          var endButtonConatiner = document.getElementById("gmt-end-measure-button-container");

          if (endButtonConatiner) {
            endButtonConatiner.style.width = "0%";
            endButtonConatiner.style.display = "none";
          }

          if (!this._fullScreen) this._setFullScreen();
        }

        if (points && points.length > 1) {
          this.mainPage = true;
        }

        this._showNotification('step2');

        var controlsContainer = document.getElementById("controls-container");

        if (controlsContainer) {
          this.container.removeChild(controlsContainer);
        }

        this.map.setOptions({
          zoomControl: true
        });
      }
    }
  }, {
    key: "endMeasure",
    value: function endMeasure(cancel) {
      if (this._measureTool) {
        this._currentMeasureCancelled = cancel;
        this._autoPolygonNotFound = cancel;

        this._measureTool.end();
      }
    }
  }, {
    key: "onEndMeasure",
    value: function onEndMeasure(event, target) {
      if (!target._currentMeasureCancelled && event.result.points && event.result.points.length) {
        var id = Math.random().toString(36).substring(2, 8) + Math.random().toString(36).substring(2, 8);
        target.results.push(Object.assign(event.result, {
          id: id
        }));
        var segments = event && event.result && event.result.segments;
        var pathObjects = [];
        segments.forEach(function (seg) {
          pathObjects.push(new google.maps.LatLng(seg.start_location));
          pathObjects.push(new google.maps.LatLng(seg.end_location));
        });
        var pathArray = new google.maps.MVCArray(pathObjects);
        var polyLine = new google.maps.Polyline({
          id: id,
          path: pathArray,
          strokeColor: "rgb(24,160,251)",
          strokeOpacity: 1.0,
          strokeWeight: 5,
          zIndex: 3
        });
        polyLine.setMap(target.map);
        google.maps.event.addListener(polyLine, 'click', function (e) {
          if (e.stopPropagation) e.stopPropagation();
          if (e.preventDefault) e.preventDefault();

          if (!target._contextMenu || !target._contextMenu.isDisplayed()) {
            target._openContextMenu(e, polyLine);
          }
        });
        target.polyLines.push(polyLine);

        if (target.mainPage) {
          target._defineInitialFlow();
        } else {
          target._showToolActions("measuring");
        }
      }

      if (target._autoPolygonNotFound) {
        // target.mainPage = false;

        // target._showNotification("autopolygon_not_found");

	// target._showPopup();

        // target.map.setOptions({
        //  zoomControl: false
        // });

        // target._unsetFullScreen();
      // } else if (target._currentMeasureCancelled) {
        target.mainPage = false;
        if (target._updatePolyLineMode) target._setPolyLineState();

        target._showNotification("step1");

        target._showToolActions("measuring");

        target._showHeader("edit");
      }

      target.hide();
      target._autoPolygonNotFound = false;
      target._currentMeasureCancelled = false;
      target._updatePolyLineMode = false;
    }
  }, {
    key: "onChangeMeasure",
    value: function onChangeMeasure(event, target) {
      var points = 0;

      if(document.getElementById('top_corner3').style.display == "block")
				{
				   //document.getElementById("top_corner2").style.display = "none";
				   //alert('y');
				   document.getElementById('top_corner3').style.display == "block";

				}else{
					 document.getElementById('top_corner2').style.display == "block";
					 //alert('n');
				}



      if (event && event.result && event.result.points) {
        points = event.result.points.length;
      }

      if (points < 3) {
        target.notification.hide();
      }

      if (!target.mainPage) {
        if (points < 3) {
          target.showNotifications(points);
        }
      }



      if (points > 1) {
        var cancelButtonConatiner = document.getElementById("gmt-cancel-measure-button-container");

        if (cancelButtonConatiner) {
          cancelButtonConatiner.style.width = "100%";
          cancelButtonConatiner.style.marginRight = "8px";
          cancelButtonConatiner.style.display = "flex";
        }
        document.getElementById("top_corner").style.display = "none";
        document.getElementById("bottom_text").style.display = "none";
        document.getElementById('top_corner2').style.display = "block";
        document.getElementById('top_corner3').style.display = "none";
        
        var endButtonConatiner = document.getElementById("gmt-end-measure-button-container");

        if (endButtonConatiner) {
          endButtonConatiner.style.width = "100%";
          endButtonConatiner.style.marginLeft = "8px";
          endButtonConatiner.style.display = "flex";
        }
      }
    }
  }, {
    key: "showNotifications",
    value: function showNotifications(points) {
      if (!points) {
        this._showNotification('step2');
      } else if (points === 1) {
        this._showNotification('step3');
      } else if (points > 1) {
        this._showNotification('step4');
      }
    }
  }, {
    key: "calculateTotal",
    value: function calculateTotal() {
      return this.results.reduce(function (total, currentValue) {
        return total + currentValue.length;
      }, 0);
    }
  }, {
    key: "cancel",
    value: function cancel() {
      if (this._measureTool) {
        this._currentMeasureCancelled = true;

        this._measureTool.end();
      }
    }
  }, {
    key: "hide",
    value: function hide() {
      var measureButtonsContainer = document.getElementById("gmt-measure-buttons-container");

      if (measureButtonsContainer) {
        measureButtonsContainer.style.visibility = "hidden";
      }
    }
  }, {
    key: "show",
    value: function show() {
      var measureButtonsContainer = document.getElementById("gmt-measure-buttons-container");

      if (measureButtonsContainer) {
        measureButtonsContainer.style.visibility = "visible";
      }
    }
  }, {
    key: "getMeasure",
    value: function getMeasure() {
      return {
        total: this.calculateTotal(),
        results: this.results,
        polyLines: this.polyLines
      };
    }
  }, {
    key: "reset",
    value: function reset() {
      if (this._measureTool) {
        this._measureTool.end();

        this._removePolyLines();

        this.results = [];
      }
    } //Private

  }, {
    key: "_removePolyLines",
    value: function _removePolyLines() {
      this.polyLines.forEach(function (polyLine) {
        polyLine.setMap(null);
      });
      this.polyLines = [];
    }
  }, {
    key: "_updatePolyLines",
    value: function _updatePolyLines() {
      var _this2 = this;

      this.polyLines.forEach(function (polyLine) {
        polyLine.setMap(_this2.map);
      });
    }
  }, {
    key: "_sleep",
    value: function _sleep(time) {
      return new Promise(function (resolve) {
        return setTimeout(resolve, time);
      });
    }
  }, {
    key: "_updatePolyLine",
    value: function _updatePolyLine(polyLine) {
      var path = polyLine.getPath();

      if (path) {
        var pointsArray = path.getArray();

        if (pointsArray && pointsArray.length) {
          var points = [];
          pointsArray.forEach(function (point, index) {
            var pointExists = points.find(function (p) {
              return p.lat === point.lat() && p.lng === point.lng();
            });
            if (!pointExists || pointsArray.length === index + 1) points.push({
              lat: point.lat(),
              lng: point.lng()
            });
          });
          var self = this;
          this._updatePolyLineMode = true;

          this._savePolyLineState(polyLine);

          this._removePolyLine(polyLine);

          this._showHeader("draw");

          this._sleep(100).then(function () {
            //We need to wait until the context menu is close, because the measuring library is detecting other click
            //only in desktop browser.
            self.startMeasure({
              points: points
            });
          });
        }
      }
    }
  }, {
    key: "_removePolyLine",
    value: function _removePolyLine(polyLine) {
      if (polyLine) {
        polyLine.setMap(null);
        this.polyLines = this.polyLines.filter(function (item) {
          return item.id != polyLine.id;
        });
        this.results = this.results.filter(function (item) {
          return item.id != polyLine.id;
        });
        this.toolActions.hide();

        this._showToolActions('measuring');
      }
    }
  }, {
    key: "_setMeasureState",
    value: function _setMeasureState() {
      this._removePolyLines();

      this.polyLines = [];

      if (this._polyLinesState && this._polyLinesState.length) {
        this.polyLines = this._polyLinesState.map(function (polyLine) {
          return polyLine;
        });
      }

      this.results = [];

      if (this._resultsState && this._resultsState.length) {
        this.results = this._resultsState.map(function (result) {
          return result;
        });
      }

      this._updatePolyLines();
    }
  }, {
    key: "_saveMeasureState",
    value: function _saveMeasureState() {
      this._polyLinesState = this.polyLines.map(function (polyLine) {
        return polyLine;
      });
      this._resultsState = this.results.map(function (result) {
        return result;
      });
    }
  }, {
    key: "_savePolyLineState",
    value: function _savePolyLineState(polyLine) {
      this._polyLineState = polyLine;
      var result = this.results.find(function (item) {
        return item.id === polyLine.id;
      });

      if (result) {
        this._resultState = result;
      }
    }
  }, {
    key: "_setPolyLineState",
    value: function _setPolyLineState() {
      if (this._polyLineState) {
        this.polyLines.push(this._polyLineState);
      }

      if (this._resultState) {
        this.results.push(this._resultState);
      }

      this._polyLineState = null;
      this._resultState = null;

      this._updatePolyLines();
    }
  }, {
    key: "_defineInitialFlow",
    value: function _defineInitialFlow() {
      this._showToolActions("main");

      this.notification.hide();

      if (this.mainPage) {
        this._showNotification("verify");
      }

      this.map.setOptions({
        zoomControl: false
      });
    }
  }, {
    key: "_showNotification",
    value: function _showNotification(type) {
      this.notification.hide();
      var data = this.getMeasure();
      var total = data && data.total > 0 ? data.total.toFixed(2) : 0;
      if(total && total!=0 && createAgain != true){
        // document.getElementById('get_started').style.display = 'none';
        document.getElementById('next-step').style.display = "";
      }
      if (type === "verify") {
        this.notification.show({
          //content: "\n<div style=\"color: white; text-align: center;\">\n<div class=\"gmt-h6\" style='font-weight:100;visibility:hidden;'>\nVerify our measurement of your home's gutters\n</div>\n<div class=\"gmt-h6\" style=\"border: 1px solid;background: #fff;color: #11A2E0;max-width: 160px;font-size: 22px;padding: 7px;border-top-left-radius: 15px; margin: 7px auto;font-weight: 100; border-top-right-radius: 15px;\"><span>TOTAL</span></div><br><div style='border: 1px solid; font-size: 29px; max-width: 161px; padding: 7px; background: #11A2E0; border: none; margin: -28px auto; border-bottom-left-radius: 15px; font-weight: 100; border-bottom-right-radius: 15px;' class='gmt-h6'>".concat(total, "ft</div>\n"),
          content: "\n<div style=\"color: white; text-align: center;\">\n<div class=\"gmt-h6\" style='font-weight:100;visibility:hidden;'>\nVerify our measurement of your home's gutters\n</div>\n<div class='gmt-h5 black' id='gmt-tool-actions-total'><span>Total</span><span class='fitValue'>".concat(total, "ft</span></div>\n"),

          type: 'transparent'
        });
      } else if (type === "autopolygon_not_found") {
        this.notification.show({
          content: "\n<div style=\"color: white; text-align: center;\">\n<div class=\"gmt-h6\">\nLet's measure your home\n</div>\n</div>\n",
          type: 'transparent'
        });
      } else if (type === "step1") {
        this.notification.show({
          content: "\n<div class=\"body-1\"><h2 style='color:white;text-align:center;font-size:22px;display:none;'>Tap Corner of Home</h2></div><div style='position:relative;top:455px'><h3 style='font-size:20px;color:white;text-align:center;font-weight:normal;display:none;'> Tap at the Corner of your Home to add the first gutter section.</h3></div>\n",
          top: 55,
        });
      } else if (type === "step2") {
        this.notification.show({
          content: "\n<div class=\"body-1\" style=\"font-size:22px;color:white;display:none;\">Place the First point top of the roof.</div>\n                ",
          top: 55
        });
      } else if (type === "step3") {
        this.notification.show({
          content: "\n<div class=\"body-1\" style=\"font-size:22px;color:white;display:none;\">Place the Second point top of the roof.</div>\n                ",
          top: 55
        });
      } else if (type === "step4") {
        this.notification.show({
          content: "\n<div class=\"body-1\" style=\"font-size:22px;color:white;text-align:center;display:none;\">\nAdd connections until Gutter Length is complete.\n</div>\n<div style='text-align:center;'><h3 style=\" font-size:16px;color:white;word-spacing:5px;margin-top:15px;font-weight:100;display:none; \">To adjust a point,just tap/click and drag to desired spot</h3></div>",
          top: 55
        });
      } else if (type === "step5") {
        this.notification.show({
          content: "\n<div class=\"body-1\" style=\" font-size:16px;color:white;text-align:center;display:none; \">\n<span style=\"font-size:16px;color:white;text-align:center;display:none;\">If you need to more gutter sections,</span> \nTap CREATE GUTTER SECION or Tap on Gutter paths to edit.\n</div>\n",
          top: 55
        });
      }
			/*if(total && total==null){
       setTimeout(() => {this.notification.hide();}, 15000);
      }else{
      	this.notification.hide();
      }*/
      document.getElementById('top_corner2').style.display = 'none';
      
    }
  }, {
    key: "_showToolActions",
    value: function _showToolActions(type) {
      var _this3 = this;

      if (type === "main") {
        this.toolActions.show({
          mainButtonHelpText: "Not Correct? Use our measure tool by tapping on Fix Gutter below:",
          mainButtonTitle: "\n                    <div style=\"display: flex; align-items: center;\">\n                        FIX GUTTER \n                        <span style=\"color: rgba(0, 0, 0, 0.54); margin-left: 4px\" class=\"material-icons\">\n                            arrow_forward\n                        </span>\n                    </div>\n                ",
          mainButtonCallback: function mainButtonCallback() {
            _this3.mainPage = false;

            _this3._showModal();
          }
        });
      } else if (type === "measuring") {
        var data = this.getMeasure();
        this.toolActions.show({
          data: data,
          floatButtonCallback: function floatButtonCallback() {
            _this3.startMeasure({});

            _this3.toolActions.hide();

            _this3._showHeader("draw");
          },
          mainButtonTitle: "\n                    <button class=\"gmt-btn gmt-btn-".concat(data.total ? 'blue' : 'disabled', "\" style='display:none'>\n                        ALL GUTTER SECTIONS MEASURED\n                    </button>\n                "),
          mainButtonCallback: function mainButtonCallback() {
            if (_this3.polyLines.length > 0) {
              _this3.saveMeasure();

              _this3._unsetFullScreen();
            }
          }
        });
      }
    }
  }, {
    key: "_showModal",
    value: function _showModal() {
      var _this4 = this;

      var modal = new _modal["default"]();
      modal.show({
        title: "Gutter Glove Measure Tool",
        message: "Tap and Drag nodes to change their location. Tap on nodes to delete and add new ones.",
        titleButton: "LET'S GET STARTED",
        callback: function callback() {
          _this4._saveMeasureState();

          _this4.reset();

          _this4._showNotification("step1");

          _this4._showToolActions("measuring");

          _this4.toolActions.hide();

          _this4._showHeader("edit");

          if (!_this4._fullScreen) _this4._setFullScreen();
        }
      });
    }
  }, {
    key: "_showPopup",
    value: function _showPopup() {
      var _this5 = this;

      var mainContainer = document.getElementById(this._el);

      if (mainContainer && mainContainer.parentElement) {
        this._parentBottom = mainContainer.parentElement.style.bottom;
        mainContainer.parentElement.style.bottom = "0px";
      }

      var popup = new _popup["default"]();
      popup.show({
        content: "\n                <div style=\"color: white; text-align: center; padding: 59px 55px 60px 55px; width: 215px; height: 215px;\">\n                    <span style=\"font-size: 56px; color: white; padding-bottom: 16px;\" class=\"material-icons\">touch_app</span>\n                    <div class=\"gmt-h6\">Get Started</div>\n                </div>\n            ",
        callback: function callback() {
          _this5._showNotification("step1");

          _this5._showToolActions("measuring");

          _this5._showHeader("edit");

          if (!_this5._fullScreen) _this5._setFullScreen();
        }
      });
    }
  }, {
    key: "_showHeader",
    value: function _showHeader(type) {
      var _this6 = this;

      this.header.hide();

      if (type === "edit") {
        var options = {
          title: "Edit Measurement "
        };

        if (this._polyLinesState && this._polyLinesState.length) {
          options.actionText = "CANCEL";

          options.actionCallback = function () {
            _this6._setMeasureState();

            _this6.saveMeasure();

            _this6._unsetFullScreen();
          };
        }

        this.header.show(options);
      } else if (type === "draw") {
        this.header.show({
          title: "Trace Gutter"
        });
      }
    }
  }, {
    key: "_openContextMenu",
    value: function _openContextMenu(e, polyLine) {
      var _this7 = this;

      var self = this;

      if (this.map && !this._updatePolyLineMode) {
        var point = e.latLng;
        this._contextMenu = new _contextMenu["default"](new google.maps.OverlayView(), this.map);

        this._contextMenu.show([{
          content: "\n                        <span style=\"font-size: 18px; color: rgba(0, 0, 0, 0.54);margin: 0px 26px 0px 30px;\" class=\"material-icons\">\n                            create\n                        </span>\n                        <span class=\"gmt-context-menu-text\">Edit</span>\n                    ",
          callback: function callback() {
            _this7._updatePolyLine(polyLine);
          }
        }, {
          content: "\n                        <span style=\"font-size: 18px; color: rgba(0, 0, 0, 0.54);margin: 0px 26px 0px 30px;\" class=\"material-icons\">\n                            delete_outline\n                        </span>\n                        <span class=\"gmt-context-menu-text\">Delete</span>\n                    ",
          callback: function callback() {
            _this7._removePolyLine(polyLine);
          }
        }], point, this);

        var cmMapClick = this.map.addListener('click', function () {
          if (self._contextMenu) self._contextMenu.hide();
          cmMapClick.remove();
        });
      }
    }
  }, {
    key: "_setFullScreen",
    value: function _setFullScreen() {
      var mainContainer = document.getElementById(this._el);

      if (mainContainer) {
        if (mainContainer.style.height) {
          this._originalHeight = mainContainer.style.height;
        } else {
          this._originalHeight = mainContainer.offsetHeight;
        }

        mainContainer.style.height = "100%";
        mainContainer.style.position = "relative";
        this._fullScreen = true;
      }
    }
  }, {
    key: "_unsetFullScreen",
    value: function _unsetFullScreen() {
      var mainContainer = document.getElementById(this._el);

      if (mainContainer && this._originalHeight) {
        mainContainer.style.height = Number.isInteger(this._originalHeight) ? "".concat(this._originalHeight, "px") : this._originalHeight;
        mainContainer.style.position = "absolute";
        this._fullScreen = false;
      }
    }
  }]);
  return Measure;
}();

exports["default"] = Measure;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(25);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(4)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../node_modules/css-loader/index.js!../node_modules/sass-loader/lib/loader.js!./styles.scss", function() {
			var newContent = require("!!../node_modules/css-loader/index.js!../node_modules/sass-loader/lib/loader.js!./styles.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 9 */
/***/ (function(module, exports) {

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

module.exports = _asyncToGenerator;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(29);


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

var _contextMenu = _interopRequireDefault(__webpack_require__(30));

var ContextMenu = /*#__PURE__*/function () {
  function ContextMenu(overlay, map) {
    (0, _classCallCheck2["default"])(this, ContextMenu);
    this._map = map;
    this._overlay = overlay;
    this._overlay.onAdd = this._onAdd.bind(this);
    this._overlay.draw = this._draw.bind(this);
    this._overlay.onRemove = this._onRemove.bind(this);
    this.displayed = false;
    this.hide();
  }

  (0, _createClass2["default"])(ContextMenu, [{
    key: "show",
    value: function show(options, position, context) {
      var _this = this;

      this.position = position;
      this.containerDiv = document.createElement('div');
      this.containerDiv.id = 'gmt-context-menu-container';
      this.containerDiv.classList.add('gmt-context-menu-container');
      this.containerDiv.stylesheet = _contextMenu["default"];

      this.containerDiv.oncontextmenu = function (event) {
        return event.preventDefault();
      };

      if (options && options.length) {
        options.forEach(function (option) {
          var item = document.createElement("div");
          item.id = "gmt-context-menu-item";
          item.className = "gmt-context-menu-item";
          item.innerHTML = option.content;

          item.onclick = function (e) {
            e.preventDefault();
            if (option.callback) option.callback.apply(context);

            _this.hide();
          };

          _this.containerDiv.appendChild(item);
        });
      }

      this._overlay.setMap(this._map);

      this.displayed = true;

      this._map.panTo(this.position);
    }
  }, {
    key: "hide",
    value: function hide() {
      var cm = document.getElementById('gmt-context-menu-container');
      if (cm) cm.remove();
      this.displayed = false;
      if (this._overlay) this._overlay.setMap(null);
    }
  }, {
    key: "isDisplayed",
    value: function isDisplayed() {
      return this.displayed;
    }
  }, {
    key: "_onAdd",
    value: function _onAdd() {
      this._overlay.getPanes().floatPane.appendChild(this.containerDiv);
    }
  }, {
    key: "_onRemove",
    value: function _onRemove() {
      if (this.containerDiv.parentElement) {
        this.containerDiv.parentElement.removeChild(this.containerDiv);
      }
    }
  }, {
    key: "_draw",
    value: function _draw() {
      var divPosition = this._overlay.getProjection().fromLatLngToDivPixel(this.position); // Hide the popup when it is far out of view.


      var display = Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ? "block" : "none";

      if (display === "block") {
        this.containerDiv.style.left = divPosition.x + "px";
        this.containerDiv.style.top = divPosition.y + "px";
      }

      if (this.containerDiv.style.display !== display) {
        this.containerDiv.style.display = display;
      }
    }
  }]);
  return ContextMenu;
}();

exports["default"] = ContextMenu;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

var Header = /*#__PURE__*/function () {
  function Header(parentContainer) {
    (0, _classCallCheck2["default"])(this, Header);
    this._parentContainer = parentContainer;
    this._id = Date.now();
  }

  (0, _createClass2["default"])(Header, [{
    key: "show",
    value: function show(_ref) {
      var title = _ref.title,
          actionText = _ref.actionText,
          actionCallback = _ref.actionCallback;
      var headerContainer = document.createElement("div");
      headerContainer.id = "gmt-header-" + this._id;
      headerContainer.className = "gmt-header";
      if (this._parentContainer) this._parentContainer.appendChild(headerContainer);
      var action = document.createElement('div');
      action.style.width = "80px";

      if (actionCallback) {
        action.id = "gmt-header-action";
        action.className = "gmt-header-action";
        action.innerHTML = actionText;

        action.onclick = function (e) {
          e.preventDefault();

          if (actionCallback) {
            actionCallback();
          }
        };
      }

      headerContainer.appendChild(action);
      var titleEl = document.createElement('div');

      if (title) {
        titleEl.id = "gmt-header-title";
        titleEl.className = "gmt-h6";
        titleEl.innerHTML = title;
      }

      headerContainer.appendChild(titleEl);
      var emptyDiv = document.createElement('div');
      emptyDiv.style.width = "80px";
      headerContainer.appendChild(emptyDiv);
    }
  }, {
    key: "hide",
    value: function hide() {
      var container = document.getElementById('gmt-header-' + this._id);
      if (container) container.remove();
    }
  }]);
  return Header;
}();

exports["default"] = Header;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

__webpack_require__(8);

var _mapTool = _interopRequireDefault(__webpack_require__(6));

var _measure = _interopRequireDefault(__webpack_require__(7));

var _geojsonTool = _interopRequireDefault(__webpack_require__(5));

var GutterMeasuringTool = /*#__PURE__*/function () {
  function GutterMeasuringTool(el) {
    (0, _classCallCheck2["default"])(this, GutterMeasuringTool);
    this._el = el;
    this._measure = null;
    var container = document.getElementById(this._el);

    if (!container) {
      throw new Error("Element ".concat(this._el, " not found"));
    }

    this._parentContainer = document.createElement("div");
    this._parentContainer.id = "gmt-container";
    this._parentContainer.className = "gmt-container";
    container.appendChild(this._parentContainer);
  }

  (0, _createClass2["default"])(GutterMeasuringTool, [{
    key: "show",
    value: function show(data) {
      this._load(data);
    }
  }, {
    key: "hide",
    value: function hide() {
      var el = document.getElementById(this._el);
      if (el) el.classList.remove("gmt-app");
      var gmtContainer = document.getElementById("gmt-container");
      if (gmtContainer) gmtContainer.innerHTML = "";
      if (this._measure) this._measure.reset();
    }
  }, {
    key: "getData",
    value: function getData() {
      if (!this._measure) return null;
      return this._measure.getMeasure();
    } //Private

  }, {
    key: "_load",
    value: function _load(latLng) {
      var container = document.getElementById(this._el);
      if (container) container.className = "gmt-app";
      var mapTool = new _mapTool["default"]({
        parentContainer: this._parentContainer
      });
      var map = mapTool.load();
      this._measure = new _measure["default"]({
        parentContainer: this._parentContainer,
        map: map,
        el: this._el
      });

      this._measure.load();

      var geoJsonTool = new _geojsonTool["default"]({
        parentContainer: this._parentContainer,
        measure: this._measure,
        mapTool: mapTool,
        map: map
      });
      geoJsonTool.setPolygon(latLng);
    }
  }]);
  return GutterMeasuringTool;
}();

exports["default"] = GutterMeasuringTool;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

__webpack_require__(31);

var Loading = /*#__PURE__*/function () {
  function Loading(parentContainer) {
    (0, _classCallCheck2["default"])(this, Loading);
    this._parentContainer = parentContainer;
    this._id = Date.now();
  }

  (0, _createClass2["default"])(Loading, [{
    key: "show",
    value: function show() {
      this._create();
    }
  }, {
    key: "hide",
    value: function hide() {
      var container = document.getElementById('gmt-loading-container-' + this._id);
      if (container) container.remove();
    }
  }, {
    key: "update",
    value: function update(progress) {
      if (this._indicator) {
        this._indicator.style.width = "".concat(progress, "%");
      }
    }
  }, {
    key: "_create",
    value: function _create() {
      var container = document.createElement('div');
      container.id = 'gmt-loading-container-' + this._id;
      container.classList.add('gmt-loading-container');
      var loading = document.createElement('div');
      loading.id = 'gmt-loading';
      loading.className = 'gmt-loading';
      var text = document.createElement('div');
      text.id = 'gmt-loading-text';
      text.className = 'gmt-loading-text';
      text.innerHTML = "Locating your Home";
      loading.appendChild(text);
      var bar = document.createElement('div');
      bar.className = 'gmt-loading-bar';
      loading.appendChild(bar);
      this._indicator = document.createElement('div');
      this._indicator.className = 'gmt-loading-bar-indicator';
      bar.appendChild(this._indicator);
      container.appendChild(loading);
      if (this._parentContainer) this._parentContainer.appendChild(container);
    }
  }]);
  return Loading;
}();

exports["default"] = Loading;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

__webpack_require__(32);

var Modal = /*#__PURE__*/function () {
  function Modal() {
    (0, _classCallCheck2["default"])(this, Modal);
    this.id = Date.now();
    this.modal = null;
  }

  (0, _createClass2["default"])(Modal, [{
    key: "show",
    value: function show(options) {
      this._create(options);
    }
  }, {
    key: "hide",
    value: function hide() {
      var container = document.getElementById('gmt-modal-container-' + this.id);
      if (container) container.remove();
    }
  }, {
    key: "_create",
    value: function _create(options) {
      var _this = this;

      var container = document.createElement('div');
      container.id = 'gmt-modal-container-' + this.id;
      container.classList.add('gmt-modal-container');
      document.body.appendChild(container);
      this.modal = document.createElement('div');
      this.modal.id = 'gmt-modal-' + this.id;
      this.modal.className = 'gmt-modal';

      if (options.title) {
        var header = document.createElement('div');
        header.id = 'gmt-modal-header';
        header.className = 'gmt-modal-header';
        header.innerHTML = options.title;
        this.modal.appendChild(header);
      }

      if (options.message) {
        var body = document.createElement('div');
        body.id = 'gmt-modal-body';
        body.className = 'gmt-modal-body';
        body.innerHTML = options.message;
        this.modal.appendChild(body);
      }

      if (options.titleButton) {
        var footer = document.createElement('div');
        footer.className = 'gmt-modal-footer';
        var button = document.createElement('div');
        button.className = 'gmt-div-action';
        button.innerHTML = options.titleButton;

        button.onclick = function (e) {
          e.preventDefault();

          if (options.callback) {
            options.callback();
          }

          _this.hide();
        };

        footer.appendChild(button);
        this.modal.appendChild(footer);
      }

      container.appendChild(this.modal);
    }
  }]);
  return Modal;
}();

exports["default"] = Modal;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

__webpack_require__(33);

var Notification = /*#__PURE__*/function () {
  function Notification(parentContainer) {
    (0, _classCallCheck2["default"])(this, Notification);
    this._parentContainer = parentContainer;
    this._id = Date.now();
    this._notification = null;
  }

  (0, _createClass2["default"])(Notification, [{
    key: "show",
    value: function show(options) {
      this._create(options);
    }
  }, {
    key: "hide",
    value: function hide() {
      var container = document.getElementById('gmt-notification-container-' + this._id);
      if (container) container.remove();
    }
  }, {
    key: "_create",
    value: function _create(options) {
      var container = document.createElement('div');
      container.id = 'gmt-notification-container-' + this._id;
      container.classList.add('gmt-notification-container');
      container.classList.add("gmt-notification-container-".concat(options.type ? options.type : 'default'));
      container.style.background = 'linear-gradient(180deg,black,transparent)';
      if (options.top) {
        container.style.top = "".concat(options.top, "px");
      }

      if (this._parentContainer) this._parentContainer.appendChild(container);
      this._notification = document.createElement('div');
      this._notification.id = 'notification-' + this._id;
      this._notification.className = 'gmt-notification';

      if (options.content) {
        var div = document.createElement('div');
        div.id = 'gmt-notification-content';
        div.className = 'gmt-notification-content';
        div.innerHTML = options.content;

        this._notification.appendChild(div);
      }

      this._notification.className += ' gmt-notification-' + (options.type ? options.type : 'default');
      var notificationContainer = document.getElementById('gmt-notification-container-' + this._id);
      if (notificationContainer) notificationContainer.appendChild(this._notification);
    }
  }]);
  return Notification;
}();

exports["default"] = Notification;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

__webpack_require__(34);

var Popup = /*#__PURE__*/function () {
  function Popup() {
    (0, _classCallCheck2["default"])(this, Popup);
    this._id = Date.now();
  }

  (0, _createClass2["default"])(Popup, [{
    key: "show",
    value: function show(options) {
      this._create(options);
    }
  }, {
    key: "hide",
    value: function hide() {
      var container = document.getElementById('gmt-popup-container-' + this._id);
      if (container) container.remove();
    }
  }, {
    key: "_create",
    value: function _create(options) {
      var _this = this;

      var container = document.createElement('div');
      container.id = 'gmt-popup-container-' + this._id;
      container.classList.add('gmt-popup-container');
      var mainContainer = document.getElementById("gmt-container");

      if (mainContainer) {
        mainContainer.appendChild(container);
      } else {
        document.body.appendChild(container);
      }

      var modal = document.createElement('div');
      modal.id = 'gmt-popup-' + this._id;
      modal.className = 'gmt-popup';

      if (options.content) {
        var content = document.createElement('div');
        content.id = 'gmt-popup-content';
        content.className = 'gmt-popup-content';
        content.innerHTML = options.content;

        if (options.callback) {
          content.onclick = function (e) {
            e.preventDefault();

            if (options.callback) {
              options.callback();
            }

            _this.hide();
          };
        }

        modal.appendChild(content);
      }

      container.appendChild(modal);
    }
  }]);
  return Popup;
}();

exports["default"] = Popup;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

var _toast = _interopRequireDefault(__webpack_require__(35));

var Toast = /*#__PURE__*/function () {
  function Toast() {
    (0, _classCallCheck2["default"])(this, Toast);
    this._id = Date.now();
  }

  (0, _createClass2["default"])(Toast, [{
    key: "show",
    value: function show(options) {
      this._create(options);
    }
  }, {
    key: "hide",
    value: function hide() {
      var toast = document.getElementById("toast-".concat(this._id));

      if (toast) {
        toast.className += ' cooltoast-fadeOut';
        toast.addEventListener('animationend', this._removeToast, false);
      }
    }
  }, {
    key: "_create",
    value: function _create(options) {
      // Toast container
      var container = document.createElement('div');
      container.id = 'cooltoast-container-' + this._id;
      container.className = 'cooltoast-container';
      container.stylesheet = _toast["default"];
      document.body.appendChild(container);
      var toast = document.createElement('div');
      toast.id = 'toast-' + this.id;
      toast.className = 'cooltoast-toast'; // title

      if (options.title) {
        var h4 = document.createElement('h4');
        h4.className = 'cooltoast-title';
        h4.innerHTML = options.title;
        toast.appendChild(h4);
      } // text


      if (options.text) {
        var text = document.createElement('div');
        text.id = "cooltoast-text";
        text.className = 'cooltoast-text';
        text.innerHTML = options.text;
        toast.appendChild(text);
      } // autohide


      if (options.timeout) {
        setTimeout(this.hide, options.timeout);
      }

      if (options.type) {
        toast.className += ' cooltoast-' + options.type;
      }

      var action = document.createElement('div');
      action.id = "cooltoast-action";
      action.className = 'cooltoast-action';
      action.innerHTML = "DISMISS";
      action.addEventListener('click', this.hide);
      toast.appendChild(action);
      container.appendChild(toast);
    }
  }, {
    key: "_removeToast",
    value: function _removeToast() {
      var cooltoastContainer = document.getElementById('cooltoast-container-' + this.id);
      if (cooltoastContainer) cooltoastContainer.remove();
    }
  }]);
  return Toast;
}();

exports["default"] = Toast;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(__webpack_require__(0));

var _createClass2 = _interopRequireDefault(__webpack_require__(1));

__webpack_require__(36);

var ToolActions = /*#__PURE__*/function () {
  function ToolActions(parentContainer) {
    (0, _classCallCheck2["default"])(this, ToolActions);
    this._parentContainer = parentContainer;
    this._id = Date.now();
  }

  (0, _createClass2["default"])(ToolActions, [{
    key: "show",
    value: function show(options) {
      this._create(options);
    }
  }, {
    key: "hide",
    value: function hide() {
      var container = document.getElementById('gmt-tool-actions-main-container-' + this._id);

      if (container) {
        container.remove();
      }
    }
  }, {
    key: "_createElement",
    value: function _createElement(_ref) {
      var type = _ref.type,
          id = _ref.id,
          className = _ref.className;
      var elment = document.createElement(type);
      elment.id = id;
      elment.className = className;
      return elment;
    }
  }, {
    key: "_create",
    value: function _create(options) {
      var _this = this;

      var mainContainer = this._createElement({
        type: 'div',
        id: 'gmt-tool-actions-main-container-' + this._id,
        className: 'gmt-tool-actions-main-container'
      });

      if (this._parentContainer) this._parentContainer.appendChild(mainContainer);

      if (options.floatButtonCallback) {
        var floatButtonContainer = document.createElement('div');
        floatButtonContainer.className = 'gmt-tool-actions-float-button-container';
        var floatButton = document.createElement('button');
        floatButton.className = 'gmt-btn gmt-tool-actions-float-button';
        floatButton.id = 'create_gutter_section';
        floatButton.style.backgroundColor = "#11a2e0";
        floatButton.style.borderColor = "#11a2e0";
        floatButton.style.color = "white";
        floatButton.style.borderRadius = "25px";
        floatButton.style.width = "280px";
        floatButton.style.paddingTop = '10px';
        floatButton.style.paddingBottom = '10px';
        floatButton.style.paddingRight = '16px';
        floatButton.style.paddingLeft = '16px';
        floatButton.style.display = 'none';
        floatButton.innerHTML = "\n                <span style=\"margin-right: 6px;\" class=\"material-icons\">add</span>\n                CREATE GUTTER SECTION  \n            ";

        floatButton.onclick = function (e) {
          e.preventDefault();

          if (options.floatButtonCallback) {
            options.floatButtonCallback();
          }

          _this.hide();
        };

        floatButtonContainer.appendChild(floatButton);
        mainContainer.appendChild(floatButtonContainer);
      }
      var addOneMoreButton = document.getElementById('create_btn_new');
      if(addOneMoreButton){
      	addOneMoreButton.addEventListener("click",function(e){
      			e.preventDefault();
 
          if (options.floatButtonCallback) {
            options.floatButtonCallback();
          }
          	document.getElementById('create_btn_new').style.display = 'none';
          	createAgain = true;
         		document.getElementById('next-step').style.display = 'none';
         		
					  document.getElementById("top_corner2").style.display = "none";
					  document.getElementById("top_corner3").style.display = "block";
      	});
      }
      var container = document.createElement('div');
      container.id = 'gmt-tool-actions-container';
      container.classList.add('gmt-tool-actions-container');
      mainContainer.appendChild(container);
      var actionsContainer = document.createElement('div');
      actionsContainer.style.width = "100%";

      if (options.data) {
        actionsContainer.style.width = "";
        var total = options.data.total > 0 ? options.data.total.toFixed(2) : 0;
        var resultContainer = document.createElement('div');
        resultContainer.id = 'gmt-tool-actions-result';
        resultContainer.style.display='none';
        resultContainer.style.padding = '16px';
        resultContainer.className = 'gmt-tool-actions-result';
        resultContainer.innerHTML = "\n<div class=\"gmt-caption grey\" style=\"margin-bottom: 4px\">Total Gutter Length</div>\n<div class=\"gmt-h5 black\" id=\"gmt-tool-actions-total\" ><span>Total</span><span class='fitValue'>".concat(total, "ft</span></div>\n            ");
        container.appendChild(resultContainer);
      }

      actionsContainer.id = 'gmt-tool-actions';
      actionsContainer.className = 'gmt-tool-actions';

      if (options.mainButtonTitle) {
        if (options.mainButtonHelpText) {
          var mainButtonHelpTextContainer = document.createElement('div');
          7;
          mainButtonHelpTextContainer.className = "gmt-subtitle-1";
          mainButtonHelpTextContainer.style.color = '#120D08';
          mainButtonHelpTextContainer.style.padding = '16px';
          mainButtonHelpTextContainer.innerHTML = options.mainButtonHelpText;
          actionsContainer.appendChild(mainButtonHelpTextContainer);
        } else {
          actionsContainer.style.display = "flex";
          actionsContainer.style.flexDirection = "column";
          actionsContainer.style.justifyContent = "flex-end";
        }

        var mainButtonContainer = document.createElement('div');
        mainButtonContainer.id = "gmt-tool-action";
        mainButtonContainer.className = "gmt-tool-action";
        mainButtonContainer.style.textAlign = "right";
        mainButtonContainer.style.padding = '0px 16px 16px 16px';
        mainButtonContainer.innerHTML = options.mainButtonTitle;

        mainButtonContainer.onclick = function (e) {
          e.preventDefault();

          if (options.mainButtonCallback) {
            options.mainButtonCallback();
          }
        };

        actionsContainer.appendChild(mainButtonContainer);
      }

      container.appendChild(actionsContainer);
    }
  }]);
  return ToolActions;
}();

exports["default"] = ToolActions;
module.exports = exports.default;
module.exports.default = exports.default;

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, ".gmt-context-menu-container {\n  outline: none;\n  position: absolute;\n  background: #FFFFFF;\n  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.48);\n  border-radius: 3px;\n  transform: translate3d(0, 0, 0);\n  max-width: 265px;\n  z-index: 1;\n  outline-offset: -2px;\n  padding: 8px 0px;\n  white-space: nowrap;\n  cursor: default;\n  border-radius: 3px; }\n\n.gmt-context-menu-item {\n  padding: 10px 0px;\n  cursor: pointer;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  align-content: space-between; }\n\n.gmt-context-menu-text {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 28px;\n  letter-spacing: 0.44px;\n  color: #000000;\n  padding-right: 40px; }\n", ""]);

// exports


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, ".gmt-loading-container {\n  position: absolute;\n  top: 0px;\n  bottom: 0px;\n  right: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 9999999;\n  background: rgba(33, 33, 33, 0.6);\n  display: flex;\n  flex-direction: column;\n  align-content: center; }\n\n.gmt-loading {\n  margin: auto 0;\n  padding: 15px; }\n\n.gmt-loading-text {\n  color: white;\n  font-size: 18px;\n  font-weight: 700; }\n\n.gmt-loading-bar {\n  position: relative;\n  background: rgba(71, 192, 187, 0.3);\n  width: 100%;\n  height: 4px; }\n\n.gmt-loading-bar-indicator {\n  background: #47C0BB;\n  height: 4px;\n  transition: width 0.5s;\n  position: absolute;\n  top: 0;\n  right: 100%;\n  bottom: 0;\n  left: 0;\n  width: 0;\n  animation: borealisBar 2s linear infinite; }\n\n@keyframes borealisBar {\n  0% {\n    left: 0%;\n    right: 100%;\n    width: 0%; }\n  10% {\n    left: 0%;\n    right: 75%;\n    width: 25%; }\n  90% {\n    right: 0%;\n    left: 75%;\n    width: 25%; }\n  100% {\n    left: 100%;\n    right: 0%;\n    width: 0%; } }\n", ""]);

// exports


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, ".gmt-modal-container {\n  position: fixed;\n  top: 0px;\n  bottom: 0px;\n  right: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 999999;\n  background: rgba(33, 33, 33, 0.6);\n  display: flex;\n  flex-direction: column;\n  align-content: center;\n  align-items: center; }\n\n.gmt-modal {\n  background: white;\n  align-self: center;\n  margin: auto 28px;\n  box-shadow: 0px 12px 16px rgba(0, 0, 0, 0.58);\n  border-radius: 6px; }\n\n.gmt-modal-header {\n  font-size: 16px;\n  font-weight: bold;\n  padding: 24px;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 20px;\n  line-height: 24px;\n  letter-spacing: 0.15px;\n  color: #212121; }\n\n.gmt-modal-body {\n  padding: 0px 24px 24px;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 28px;\n  letter-spacing: 0.44px;\n  color: rgba(33, 33, 33, 0.6); }\n\n.gmt-modal-footer {\n  padding: 18px 16px;\n  text-align: right;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  letter-spacing: 1.25px;\n  text-transform: uppercase;\n  color: #120D08; }\n", ""]);

// exports


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, ".gmt-notification-container {\n  position: absolute;\n  top: 0px;\n  right: 0;\n  left: 0;\n  width: 100%;\n  z-index: 999999; }\n\n.gmt-notification-container-transparent {\n  height: 230px;\n  padding: 24px;\n  background: linear-gradient(180deg, #000000 24%, rgba(0, 0, 0, 0) 100%); }\n\n.gmt-notification {\n  position: relative; }\n\n.gmt-notification-content {\n  font-size: 14px;\n  margin: 0px; }\n\n.gmt-notification-default {\n  background: transparent;\n  margin: 16px;\n  padding: 16px; }\n\n.gmt-notification-transparent {\n  background: transparent; }\n\n.body-1 {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 28px;\n  letter-spacing: 0.44px; }\n", ""]);

// exports


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, ".gmt-popup-container {\n  position: absolute;\n  top: 0px;\n  bottom: 0px;\n  right: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 999999;\n  display: flex;\n  flex-direction: column;\n  align-content: center;\n  align-items: center; }\n\n.gmt-popup {\n  width: 215px;\n  height: 215px;\n  background: rgba(33, 33, 33, 0.6);\n  align-self: center;\n  margin: auto 0;\n  border: 1px solid #FFFFFF;\n  box-sizing: border-box;\n  opacity: 0.8;\n  border-radius: 16px;\n  cursor: pointer; }\n\n.gmt-popup-fadeOut {\n  animation-name: gmtPopupFadeOut;\n  animation-duration: .3s;\n  animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  animation-fill-mode: forwards; }\n\n@keyframes gmtPopup {\n  from {\n    transform: translate3d(400px, 0, 0);\n    opacity: 0; }\n  to {\n    transform: translate3d(0, 0, 0);\n    opacity: 1; } }\n\n@keyframes gmtPopupFadeOut {\n  from {\n    transform: translate3d(0, 0, 0);\n    opacity: 1; }\n  to {\n    transform: translate3d(400px, 0, 0);\n    opacity: 0; } }\n", ""]);

// exports


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/icon?family=Material+Icons);", ""]);

// module
exports.push([module.i, ".gmt-app {\n  position: absolute;\n  top: 0px;\n  z-index: 999; }\n\n.gmt-container {\n  position: relative;\n  width: 100%;\n  height: 100%; }\n\n.hide {\n  display: none; }\n\n.show {\n  display: block; }\n\n#map {\n  width: 100%;\n  height: 100%; }\n\n.gmt-p-16 {\n  padding: 16px; }\n\n.bg-white {\n  background-color: white; }\n\n.all-width {\n  width: 100%; }\n\n.gmt-text-help {\n  font-size: 12px;\n  color: grey; }\n\n/* Header */\n.gmt-header {\n  position: absolute;\n  width: 100%;\n  top: 0px;\n  padding: 16px;\n  background-color: white;\n  color: black;\n  display: flex;\n  flex-direction: row;\n  place-content: space-between;\n  z-index: 999999;\n  height: 55px; }\n\n.gmt-header span {\n  display: flex;\n  align-self: center;\n  cursor: pointer; }\n\n.gmt-header-action {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  display: flex;\n  align-items: center;\n  letter-spacing: 1.25px;\n  text-transform: uppercase;\n  color: #000000; }\n\n/** Get Started */\n.gmt-get-started {\n  padding: 15px;\n  border: 1px solid #ddd;\n  margin: 5px;\n  position: absolute;\n  top: 50%;\n  -ms-transform: translateY(-50%);\n  transform: translateY(-50%);\n  background-color: white; }\n\n.gmt-get-started h4 {\n  margin-top: 15px; }\n\n/** Measure */\n#gmt-measure-buttons-container {\n  position: relative;\n  bottom: 70px;\n  padding: 16px;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  z-index: 1; }\n\n/* Buttons */\n.gmt-btn {\n  display: inline-block;\n  vertical-align: middle;\n  text-align: center;\n  cursor: pointer;\n  transition: all 300ms ease-in-out;\n  border-radius: 4px;\n  font-weight: 600; }\n\n.gmt-btn-round {\n  box-shadow: 0px 3px 4.65px rgba(0, 0, 0, 0.27);\n  border-radius: 100px;\n  width: 40px;\n  height: 40px;\n  text-align: center;\n  padding: 0px !important;\n  margin: 0px !important; }\n\n.gmt-btn-primary {\n  background-color: #47C0BB;\n  border: 0px;\n  color: #fff;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  text-align: center;\n  letter-spacing: 1.25px;\n  text-transform: uppercase; }\n\n.gmt-btn-primary:active {\n  background-color: #47C0BB;\n  box-shadow: 0 5px #77c2bf;\n  transform: translateY(4px); }\n\n.gmt-btn-danger {\n  background-color: #E62525;\n  border: 0px;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  text-align: center;\n  letter-spacing: 1.25px;\n  text-transform: uppercase;\n  color: #FFFFFF;\n  padding: 10px 16px; }\n\n.gmt-btn-danger:active {\n  background-color: #DC291E;\n  box-shadow: 0 5px #DC291E;\n  transform: translateY(4px); }\n\n.gmt-btn-disabled {\n  background-color: rgba(0, 0, 0, 0.12);\n  border: 0px;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  text-align: center;\n  letter-spacing: 1.25px;\n  text-transform: uppercase;\n  color: rgba(33, 33, 33, 0.3);\n  padding: 10px 16px;\n  cursor: not-allowed; }\n\n.gmt-btn-disabled:active {\n  background: rgba(0, 0, 0, 0.12);\n  box-shadow: 0 5px #131313;\n  transform: translateY(4px); }\n\n.gmt-btn-default {\n  background-color: #120D08;\n  border: 0px;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  text-align: center;\n  letter-spacing: 1.25px;\n  text-transform: uppercase;\n  color: #FFFFFF;\n  padding: 10px 16px; }\n\n.gmt-btn-default:active {\n  background-color: #000;\n  box-shadow: 0 5px #131313;\n  transform: translateY(4px); }\n\n.gmt-btn-blue {\n  background-color: #2196F3;\n  border: 0px;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  text-align: center;\n  letter-spacing: 1.25px;\n  text-transform: uppercase;\n  color: #FFFFFF;\n  padding: 10px 16px; }\n\n.gmt-btn-blue:active {\n  background-color: #2196F3;\n  box-shadow: 0 5px #131313;\n  transform: translateY(4px); }\n\n.gmt-btn-blank {\n  background-color: transparent;\n  border: 0px;\n  color: #000; }\n\n.gmt-btn-blank:active {\n  background-color: transparent;\n  box-shadow: 0 5px transparent;\n  transform: translateY(4px); }\n\n.gmt-div-action {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  text-align: right;\n  letter-spacing: 1.25px;\n  text-transform: uppercase;\n  color: #120D08;\n  cursor: pointer; }\n\n.gmt-h4 {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 34px;\n  line-height: 40px;\n  text-align: center;\n  letter-spacing: 0.25px; }\n\n.gmt-h5 {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 24px;\n  line-height: 32px; }\n\n.gmt-h6 {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 20px;\n  line-height: 24px;\n  align-items: center;\n  text-align: center;\n  letter-spacing: 0.15px; }\n\n.gmt-caption {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  letter-spacing: 0.4px; }\n\n.gmt-overline {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 10px;\n  line-height: 16px;\n  text-align: center;\n  letter-spacing: 1.5px;\n  text-transform: uppercase; }\n\n.gmt-subtitle-1 {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 20px;\n  letter-spacing: 0.15px; }\n\n.grey {\n  color: rgba(33, 33, 33, 0.6); }\n\n.black {\n  color: #000000; }\n\n@media screen and (-webkit-min-device-pixel-ratio: 0) {\n  select,\n  textarea,\n  input {\n    font-size: 16px; } }\n", ""]);

// exports


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, ".cooltoast-container {\n  position: absolute;\n  bottom: 0;\n  right: 0;\n  left: 0;\n  width: 100%;\n  z-index: 999999; }\n\n.cooltoast-toast {\n  position: relative;\n  padding: 8px 12px;\n  margin: 16px;\n  border-radius: 3px;\n  background: #ddd;\n  cursor: pointer;\n  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.08), 0 1px 3px rgba(0, 0, 0, 0.19);\n  animation-duration: .3s;\n  animation-name: cooltoast;\n  animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between; }\n\n.cooltoast-fadeOut {\n  animation-name: cooltoastFadeOut;\n  animation-duration: .3s;\n  animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  animation-fill-mode: forwards; }\n\n#cooltoast-container p,\n#cooltoast-container h4 {\n  margin: 0px; }\n\n.cooltoast-title {\n  font-weight: 700;\n  font-size: 15px;\n  margin-bottom: 0px; }\n\n.cooltoast-text {\n  font-size: 15px;\n  color: white;\n  margin: 0px; }\n\n.cooltoast-action {\n  cursor: pointer;\n  color: white;\n  font-weight: 600; }\n\n.cooltoast-icon {\n  position: absolute;\n  top: 5px;\n  left: -40px;\n  width: 50px;\n  height: 50px;\n  border-radius: 100%;\n  background: #FFF; }\n\n.cooltoast-toast a, .cooltoast-toast a:hover {\n  color: #549EDB !important;\n  text-decoration: none !important; }\n\n/** toast types */\n.cooltoast-success {\n  background-color: #51C625; }\n\n.cooltoast-warning {\n  background-color: #DB9215; }\n\n.cooltoast-error {\n  background-color: #DB0028; }\n\n.cooltoast-info {\n  background-color: #27ABDB; }\n\n@keyframes cooltoast {\n  from {\n    transform: translate3d(400px, 0, 0);\n    opacity: 0; }\n  to {\n    transform: translate3d(0, 0, 0);\n    opacity: 1; } }\n\n@keyframes cooltoastFadeOut {\n  from {\n    transform: translate3d(0, 0, 0);\n    opacity: 1; }\n  to {\n    transform: translate3d(400px, 0, 0);\n    opacity: 0; } }\n", ""]);

// exports


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, ".gmt-tool-actions-main-container {\n  position: absolute;\n  top: 0px;\n  right: 0;\n  left: 0;\n  width: 100%;\n  z-index: 999999;\n  padding: 16px; }\n\n.gmt-tool-actions-container {\n  position: relative;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  background-color: white; }\n\n.gmt-tool-actions-float-button-container {\n  position: relative;\n  z-index: 1; }\n\n.gmt-tool-actions-float-button {\n  position: absolute;\n  top: -65px;\n  right: 0px;\n  left: 0px;\n  margin: 0 auto;\n  display: flex;\n  flex-direction: row;\n  align-items: center; }\n\n.gmt-tool-actions {\n  position: relative; }\n\n.gmt-tool-action {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 16px;\n  text-align: right;\n  letter-spacing: 1.25px;\n  text-transform: uppercase;\n  color: #120D08;\n  cursor: pointer; }\n", ""]);

// exports


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

!function(t,e){ true?module.exports=e():"function"==typeof define&&define.amd?define("MeasureTool",[],e):"object"==typeof exports?exports.MeasureTool=e():t.MeasureTool=e()}(this,function(){return function(t){function e(i){if(n[i])return n[i].exports;var o=n[i]={i:i,l:!1,exports:{}};return t[i].call(o.exports,o,o.exports,e),o.l=!0,o.exports}var n={};return e.m=t,e.c=n,e.i=function(t){return t},e.d=function(t,n,i){e.o(t,n)||Object.defineProperty(t,n,{configurable:!1,enumerable:!0,get:i})},e.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(n,"a",n),n},e.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},e.p="",e(e.s=34)}([function(t,e,n){"use strict";function i(t,e){this._groups=t,this._parents=e}function o(){return new i([[document.documentElement]],U)}n.d(e,"c",function(){return U}),e.b=i;var r=n(72),a=n(73),s=n(60),u=n(54),l=n(18),c=n(59),h=n(63),f=n(65),p=n(68),d=n(75),g=n(51),v=n(67),_=n(66),m=n(74),y=n(58),A=n(57),x=n(50),b=n(20),w=n(69),C=n(52),B=n(76),E=n(61),T=n(70),k=n(64),L=n(49),M=n(62),P=n(71),j=n(53),S=n(55),O=n(9),D=n(56),U=[null];i.prototype=o.prototype={constructor:i,select:r.a,selectAll:a.a,filter:s.a,data:u.a,enter:l.a,exit:c.a,join:h.a,merge:f.a,order:p.a,sort:d.a,call:g.a,nodes:v.a,node:_.a,size:m.a,empty:y.a,each:A.a,attr:x.a,style:b.b,property:w.a,classed:C.a,text:B.a,html:E.a,raise:T.a,lower:k.a,append:L.a,insert:M.a,remove:P.a,clone:j.a,datum:S.a,on:O.c,dispatch:D.a},e.a=o},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});e.Config={prefix:"measure-tool",tooltipText1:"Drag to change, click to remove",tooltipText2:"Drag to change"}},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var i=n(45);n.d(e,"create",function(){return i.a});var o=n(4);n.d(e,"creator",function(){return o.a});var r=n(46);n.d(e,"local",function(){return r.a});var a=n(16);n.d(e,"matcher",function(){return a.a});var s=n(47);n.d(e,"mouse",function(){return s.a});var u=n(7);n.d(e,"namespace",function(){return u.a});var l=n(8);n.d(e,"namespaces",function(){return l.a});var c=n(5);n.d(e,"clientPoint",function(){return c.a});var h=n(17);n.d(e,"select",function(){return h.a});var f=n(48);n.d(e,"selectAll",function(){return f.a});var p=n(0);n.d(e,"selection",function(){return p.a});var d=n(10);n.d(e,"selector",function(){return d.a});var g=n(21);n.d(e,"selectorAll",function(){return g.a});var v=n(20);n.d(e,"style",function(){return v.a});var _=n(77);n.d(e,"touch",function(){return _.a});var m=n(78);n.d(e,"touches",function(){return m.a});var y=n(12);n.d(e,"window",function(){return y.a});var A=n(9);n.d(e,"event",function(){return A.a}),n.d(e,"customEvent",function(){return A.b})},function(t,e){t.exports=function(){var t=[];return t.toString=function(){for(var t=[],e=0;e<this.length;e++){var n=this[e];n[2]?t.push("@media "+n[2]+"{"+n[1]+"}"):t.push(n[1])}return t.join("")},t.i=function(e,n){"string"==typeof e&&(e=[[null,e,""]]);for(var i={},o=0;o<this.length;o++){var r=this[o][0];"number"==typeof r&&(i[r]=!0)}for(o=0;o<e.length;o++){var a=e[o];"number"==typeof a[0]&&i[a[0]]||(n&&!a[2]?a[2]=n:n&&(a[2]="("+a[2]+") and ("+n+")"),t.push(a))}},t}},function(t,e,n){"use strict";function i(t){return function(){var e=this.ownerDocument,n=this.namespaceURI;return n===a.b&&e.documentElement.namespaceURI===a.b?e.createElement(t):e.createElementNS(n,t)}}function o(t){return function(){return this.ownerDocument.createElementNS(t.space,t.local)}}var r=n(7),a=n(8);e.a=function(t){var e=n.i(r.a)(t);return(e.local?o:i)(e)}},function(t,e,n){"use strict";e.a=function(t,e){var n=t.ownerSVGElement||t;if(n.createSVGPoint){var i=n.createSVGPoint();return i.x=e.clientX,i.y=e.clientY,i=i.matrixTransform(t.getScreenCTM().inverse()),[i.x,i.y]}var o=t.getBoundingClientRect();return[e.clientX-o.left-t.clientLeft,e.clientY-o.top-t.clientTop]}},function(t,e){function n(t,e){for(var n=0;n<t.length;n++){var i=t[n],o=f[i.id];if(o){o.refs++;for(var r=0;r<o.parts.length;r++)o.parts[r](i.parts[r]);for(;r<i.parts.length;r++)o.parts.push(u(i.parts[r],e))}else{for(var a=[],r=0;r<i.parts.length;r++)a.push(u(i.parts[r],e));f[i.id]={id:i.id,refs:1,parts:a}}}}function i(t){for(var e=[],n={},i=0;i<t.length;i++){var o=t[i],r=o[0],a=o[1],s=o[2],u=o[3],l={css:a,media:s,sourceMap:u};n[r]?n[r].parts.push(l):e.push(n[r]={id:r,parts:[l]})}return e}function o(t,e){var n=g(),i=m[m.length-1];if("top"===t.insertAt)i?i.nextSibling?n.insertBefore(e,i.nextSibling):n.appendChild(e):n.insertBefore(e,n.firstChild),m.push(e);else{if("bottom"!==t.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");n.appendChild(e)}}function r(t){t.parentNode.removeChild(t);var e=m.indexOf(t);e>=0&&m.splice(e,1)}function a(t){var e=document.createElement("style");return e.type="text/css",o(t,e),e}function s(t){var e=document.createElement("link");return e.rel="stylesheet",o(t,e),e}function u(t,e){var n,i,o;if(e.singleton){var u=_++;n=v||(v=a(e)),i=l.bind(null,n,u,!1),o=l.bind(null,n,u,!0)}else t.sourceMap&&"function"==typeof URL&&"function"==typeof URL.createObjectURL&&"function"==typeof URL.revokeObjectURL&&"function"==typeof Blob&&"function"==typeof btoa?(n=s(e),i=h.bind(null,n),o=function(){r(n),n.href&&URL.revokeObjectURL(n.href)}):(n=a(e),i=c.bind(null,n),o=function(){r(n)});return i(t),function(e){if(e){if(e.css===t.css&&e.media===t.media&&e.sourceMap===t.sourceMap)return;i(t=e)}else o()}}function l(t,e,n,i){var o=n?"":i.css;if(t.styleSheet)t.styleSheet.cssText=y(e,o);else{var r=document.createTextNode(o),a=t.childNodes;a[e]&&t.removeChild(a[e]),a.length?t.insertBefore(r,a[e]):t.appendChild(r)}}function c(t,e){var n=e.css,i=e.media;if(i&&t.setAttribute("media",i),t.styleSheet)t.styleSheet.cssText=n;else{for(;t.firstChild;)t.removeChild(t.firstChild);t.appendChild(document.createTextNode(n))}}function h(t,e){var n=e.css,i=e.sourceMap;i&&(n+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(i))))+" */");var o=new Blob([n],{type:"text/css"}),r=t.href;t.href=URL.createObjectURL(o),r&&URL.revokeObjectURL(r)}var f={},p=function(t){var e;return function(){return void 0===e&&(e=t.apply(this,arguments)),e}},d=p(function(){return/msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase())}),g=p(function(){return document.head||document.getElementsByTagName("head")[0]}),v=null,_=0,m=[];t.exports=function(t,e){if("undefined"!=typeof DEBUG&&DEBUG&&"object"!=typeof document)throw new Error("The style-loader cannot be used in a non-browser environment");e=e||{},void 0===e.singleton&&(e.singleton=d()),void 0===e.insertAt&&(e.insertAt="bottom");var o=i(t);return n(o,e),function(t){for(var r=[],a=0;a<o.length;a++){var s=o[a],u=f[s.id];u.refs--,r.push(u)}if(t){n(i(t),e)}for(var a=0;a<r.length;a++){var u=r[a];if(0===u.refs){for(var l=0;l<u.parts.length;l++)u.parts[l]();delete f[u.id]}}}};var y=function(){var t=[];return function(e,n){return t[e]=n,t.filter(Boolean).join("\n")}}()},function(t,e,n){"use strict";var i=n(8);e.a=function(t){var e=t+="",n=e.indexOf(":");return n>=0&&"xmlns"!==(e=t.slice(0,n))&&(t=t.slice(n+1)),i.a.hasOwnProperty(e)?{space:i.a[e],local:t}:t}},function(t,e,n){"use strict";n.d(e,"b",function(){return i});var i="http://www.w3.org/1999/xhtml";e.a={svg:"http://www.w3.org/2000/svg",xhtml:i,xlink:"http://www.w3.org/1999/xlink",xml:"http://www.w3.org/XML/1998/namespace",xmlns:"http://www.w3.org/2000/xmlns/"}},function(t,e,n){"use strict";function i(t,e,n){return t=o(t,e,n),function(e){var n=e.relatedTarget;n&&(n===this||8&n.compareDocumentPosition(this))||t.call(this,e)}}function o(t,e,n){return function(i){var o=c;c=i;try{t.call(this,this.__data__,e,n)}finally{c=o}}}function r(t){return t.trim().split(/^|\s+/).map(function(t){var e="",n=t.indexOf(".");return n>=0&&(e=t.slice(n+1),t=t.slice(0,n)),{type:t,name:e}})}function a(t){return function(){var e=this.__on;if(e){for(var n,i=0,o=-1,r=e.length;i<r;++i)n=e[i],t.type&&n.type!==t.type||n.name!==t.name?e[++o]=n:this.removeEventListener(n.type,n.listener,n.capture);++o?e.length=o:delete this.__on}}}function s(t,e,n){var r=l.hasOwnProperty(t.type)?i:o;return function(i,o,a){var s,u=this.__on,l=r(e,o,a);if(u)for(var c=0,h=u.length;c<h;++c)if((s=u[c]).type===t.type&&s.name===t.name)return this.removeEventListener(s.type,s.listener,s.capture),this.addEventListener(s.type,s.listener=l,s.capture=n),void(s.value=e);this.addEventListener(t.type,l,n),s={type:t.type,name:t.name,value:e,listener:l,capture:n},u?u.push(s):this.__on=[s]}}function u(t,e,n,i){var o=c;t.sourceEvent=c,c=t;try{return e.apply(n,i)}finally{c=o}}n.d(e,"a",function(){return c}),e.b=u;var l={},c=null;if("undefined"!=typeof document){"onmouseenter"in document.documentElement||(l={mouseenter:"mouseover",mouseleave:"mouseout"})}e.c=function(t,e,n){var i,o,u=r(t+""),l=u.length;{if(!(arguments.length<2)){for(c=e?s:a,null==n&&(n=!1),i=0;i<l;++i)this.each(c(u[i],e,n));return this}var c=this.node().__on;if(c)for(var h,f=0,p=c.length;f<p;++f)for(i=0,h=c[f];i<l;++i)if((o=u[i]).type===h.type&&o.name===h.name)return h.value}}},function(t,e,n){"use strict";function i(){}e.a=function(t){return null==t?i:function(){return this.querySelector(t)}}},function(t,e,n){"use strict";var i=n(9);e.a=function(){for(var t,e=i.a;t=e.sourceEvent;)e=t;return e}},function(t,e,n){"use strict";e.a=function(t){return t.ownerDocument&&t.ownerDocument.defaultView||t.document&&t||t.defaultView}},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});e.UnitTypeId={METRIC:"metric",IMPERIAL:"imperial",NAUTICAL:"nautical"}},function(t,e,n){"use strict";function i(t,e){var i=t.document.documentElement,a=n.i(o.select)(t).on("dragstart.drag",null);e&&(a.on("click.drag",r.a,!0),setTimeout(function(){a.on("click.drag",null)},0)),"onselectstart"in i?a.on("selectstart.drag",null):(i.style.MozUserSelect=i.__noselect,delete i.__noselect)}e.b=i;var o=n(2),r=n(15);e.a=function(t){var e=t.document.documentElement,i=n.i(o.select)(t).on("dragstart.drag",r.a,!0);"onselectstart"in e?i.on("selectstart.drag",r.a,!0):(e.__noselect=e.style.MozUserSelect,e.style.MozUserSelect="none")}},function(t,e,n){"use strict";function i(){o.event.stopImmediatePropagation()}e.b=i;var o=n(2);e.a=function(){o.event.preventDefault(),o.event.stopImmediatePropagation()}},function(t,e,n){"use strict";e.a=function(t){return function(){return this.matches(t)}}},function(t,e,n){"use strict";var i=n(0);e.a=function(t){return"string"==typeof t?new i.b([[document.querySelector(t)]],[document.documentElement]):new i.b([[t]],i.c)}},function(t,e,n){"use strict";function i(t,e){this.ownerDocument=t.ownerDocument,this.namespaceURI=t.namespaceURI,this._next=null,this._parent=t,this.__data__=e}e.b=i;var o=n(19),r=n(0);e.a=function(){return new r.b(this._enter||this._groups.map(o.a),this._parents)},i.prototype={constructor:i,appendChild:function(t){return this._parent.insertBefore(t,this._next)},insertBefore:function(t,e){return this._parent.insertBefore(t,e)},querySelector:function(t){return this._parent.querySelector(t)},querySelectorAll:function(t){return this._parent.querySelectorAll(t)}}},function(t,e,n){"use strict";e.a=function(t){return new Array(t.length)}},function(t,e,n){"use strict";function i(t){return function(){this.style.removeProperty(t)}}function o(t,e,n){return function(){this.style.setProperty(t,e,n)}}function r(t,e,n){return function(){var i=e.apply(this,arguments);null==i?this.style.removeProperty(t):this.style.setProperty(t,i,n)}}function a(t,e){return t.style.getPropertyValue(e)||n.i(s.a)(t).getComputedStyle(t,null).getPropertyValue(e)}e.a=a;var s=n(12);e.b=function(t,e,n){return arguments.length>1?this.each((null==e?i:"function"==typeof e?r:o)(t,e,null==n?"":n)):a(this.node(),t)}},function(t,e,n){"use strict";function i(){return[]}e.a=function(t){return null==t?i:function(){return this.querySelectorAll(t)}}},function(t,e,n){"use strict";function i(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var o=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),r=n(1),a=n(79),s=function(t){return t&&t.__esModule?t:{default:t}}(a),u=function(){function t(e,n){i(this,t),this._defaultOptions={width:150},this._options=Object.assign({},this._defaultOptions,n||{}),this._parentDiv=e,this._containerDiv=document.createElement("div"),this._containerDiv.classList.add(r.Config.prefix+"-context-menu"),this._containerDiv.stylesheet=s.default,this._containerDiv.oncontextmenu=function(t){return t.preventDefault()},this._list=document.createElement("ul"),this._containerDiv.appendChild(this._list),e.appendChild(this._containerDiv),this._isVisible=!1}return o(t,[{key:"left",get:function(){return this._containerDiv.getBoundingClientRect().left}},{key:"top",get:function(){return this._containerDiv.getBoundingClientRect().top}},{key:"width",get:function(){return this._containerDiv.getBoundingClientRect().width}},{key:"height",get:function(){return this._containerDiv.getBoundingClientRect().height}}]),o(t,[{key:"addItem",value:function(t,e,n){var i=this,o=arguments.length>3&&void 0!==arguments[3]?arguments[3]:this,r=document.createElement("li"),a=document.createTextNode(t);return r.appendChild(a),r.onclick=function(t){t.preventDefault(),n.apply(o),i.hide()},e?this.showItem(r):this.hideItem(r),this._list.appendChild(r),r}},{key:"hideItem",value:function(t){t.style.cssText="display: none"}},{key:"showItem",value:function(t){t.style.cssText="display: block"}},{key:"toggleItems",value:function(t,e){var n=this;t.forEach(function(t){return n.showItem(t)}),e.forEach(function(t){return n.hideItem(t)})}},{key:"show",value:function(t){this._isVisible=!0,this._containerDiv.style.cssText="\n      display: block;\n      visibility: hidden;\n      position: absolute;\n      width: "+this._options.width+"px; \n    ";var e=this._parentDiv.getBoundingClientRect().width<=t.x+this.width,n=this._parentDiv.getBoundingClientRect().height<=t.y+this.height;this._containerDiv.style.cssText+="\n      "+(e?"right: 0px;":"left: "+t.x+"px;")+"\n      "+(n?"bottom: 14px;":"top: "+t.y+"px;")+"\n      visibility: visible;\n    "}},{key:"hide",value:function(){this._isVisible=!1,this._containerDiv.style.cssText="display: none"}},{key:"toggle",value:function(t){this._isVisible?this.hide():this.show(t)}}]),t}();e.default=u,t.exports=e.default},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});e.EVENT_START="measure_start",e.EVENT_END="measure_end",e.EVENT_CHANGE="measure_change"},function(t,e,n){"use strict";function i(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var o=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}();e.Geometry=function(){function t(){i(this,t),this._nodes=[]}return o(t,[{key:"nodes",get:function(){return this._nodes}},{key:"lines",get:function(){var t=[];if(this._nodes.length>1)for(var e=1;e<this._nodes.length;e++)t.push([this._nodes[e-1],this._nodes[e]]);return t}}]),o(t,[{key:"addNode",value:function(t){this._nodes.push(t)}},{key:"updateNode",value:function(t,e){this._nodes[t]=e}},{key:"removeNode",value:function(t){this._nodes.splice(t,1)}},{key:"insertNode",value:function(t,e){this._nodes.splice(t,0,e)}}]),t}()},function(t,e,n){"use strict";function i(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var o=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),r=n(13),a=function(){function t(e){i(this,t),this._options={unit:r.UnitTypeId.METRIC},Object.assign(this._options,e),this.init()}return o(t,[{key:"init",value:function(){this.initUnits()}},{key:"initUnits",value:function(){switch(this._options.unit.toLowerCase()){case r.UnitTypeId.METRIC:this._lengthMultiplier=1,this.formatLength=this._formatLengthMetric,this._areaMultiplier=1,this.formatArea=this._formatAreaMetric;break;case r.UnitTypeId.IMPERIAL:this._lengthMultiplier=3.28084,this.formatLength=this._formatLengthImperial,this._areaMultiplier=10.7639,this.formatArea=this._formatAreaImperial;break;case r.UnitTypeId.NAUTICAL:this._lengthMultiplier=1,this.formatLength=this._formatLengthNautical,this._areaMultiplier=1,this.formatArea=this._formatAreaMetric;break;default:this._lengthMultiplier=1,this.formatLength=this._formatLengthMetric,this._areaMultiplier=1,this.formatArea=this._formatAreaMetric}}},{key:"setOption",value:function(t,e){if(!this._options[t])throw new Error(t+" is not a valid option on MeasureTool helper");this._options[t]=e,this.initUnits()}},{key:"computeLengthBetween",value:function(t,e){return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(t[1],t[0]),new google.maps.LatLng(e[1],e[0]))*this._lengthMultiplier}},{key:"computePathLength",value:function(t){for(var e=0,n=1;n<t.length;n++)e+=google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(t[n-1][1],t[n-1][0]),new google.maps.LatLng(t[n][1],t[n][0]));return e*this._lengthMultiplier}},{key:"computeArea",value:function(t){return google.maps.geometry.spherical.computeArea(t.map(function(t){return new google.maps.LatLng(t[1],t[0])}))*this._areaMultiplier}},{key:"_formatLengthMetric",value:function(t){var e=void 0;return t/1e3>=1?(e="km",t/=1e3):e="m",this._numberToLocale(this._roundUp(t,2))+" "+e}},{key:"_formatLengthImperial",value:function(t){var e=void 0;return t/5280>=1?(e="mi",t/=5280):e="ft",this._numberToLocale(this._roundUp(t,2))+" "+e}},{key:"_formatLengthNautical",value:function(t){return t/=1852,this._numberToLocale(this._roundUp(t,2))+" NM"}},{key:"_formatAreaMetric",value:function(t){var e=void 0;return t/1e6>=1?(e="km²",t/=1e6):e="m²",this._numberToLocale(this._roundUp(t,2))+" "+e}},{key:"_formatAreaImperial",value:function(t){var e=void 0;return 3.587e-8*t>=1?(e="mi²",t*=3.587e-8):e="ft²",this._numberToLocale(this._roundUp(t,2))+" "+e}},{key:"_roundUp",value:function(t,e){return Number(Math.round(t+"e"+e)+"e-"+e).toFixed(e)}},{key:"_numberToLocale",value:function(t){return(new Intl.NumberFormat).format(t)}}],[{key:"findTouchPoint",value:function(t,e){var n=((t[1][1]-t[0][1])*(e[0]-t[0][0])-(t[1][0]-t[0][0])*(e[1]-t[0][1]))/(Math.pow(t[1][1]-t[0][1],2)+Math.pow(t[1][0]-t[0][0],2));return[e[0]-n*(t[1][1]-t[0][1]),e[1]+n*(t[1][0]-t[0][0])]}},{key:"findMidPoint",value:function(t){return[(t[0][0]+t[1][0])/2,(t[0][1]+t[1][1])/2]}},{key:"transformText",value:function(e,n){var i=t.findMidPoint([e,n]),o=void 0;return o=e[0]===n[0]?n[1]>e[1]?90:n[1]<e[1]?270:0:180*Math.atan((n[1]-e[1])/(n[0]-e[0]))/Math.PI,"translate("+i[0]+", "+i[1]+") rotate("+o+")"}},{key:"makeId",value:function(t){return(Math.random().toString(36)+"00000000000000000").slice(2,t+2)}},{key:"_interpolate",value:function(t,e,n){var i=google.maps.geometry.spherical.interpolate(new google.maps.LatLng(t[1],t[0]),new google.maps.LatLng(e[1],e[0]),n);return[i.lng(),i.lat()]}}]),t}();e.default=a,t.exports=e.default},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});e.ObjectAssign=function(){"function"!=typeof Object.assign&&(Object.assign=function(t){if(null===t)throw new TypeError("Cannot convert undefined or null to object");t=Object(t);for(var e=1;e<arguments.length;e++){var n=arguments[e];if(null!==n)for(var i in n)Object.prototype.hasOwnProperty.call(n,i)&&(t[i]=n[i])}return t})}},function(t,e,n){"use strict";function i(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var o=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),r=function(){function t(e,n,o){i(this,t),this._defaultOptions={offsetRate:8e3},this._options=Object.assign({},this._defaultOptions,o||{}),this._container=e,this._projection=n}return o(t,[{key:"latLngToSvgPoint",value:function(t){var e=this._options.offsetRate/2,n=new google.maps.LatLng(t[1],t[0]),i=this._projection.fromLatLngToDivPixel(n);return[i.x+e,i.y+e]}},{key:"svgPointToLatLng",value:function(t){var e=this._options.offsetRate/2,n=new google.maps.Point(t[0]-e,t[1]-e),i=this._projection.fromDivPixelToLatLng(n);return[i.lng(),i.lat()]}},{key:"svgPointToContainerPoint",value:function(t){var e=this.svgPointToLatLng(t);return this._projection.fromLatLngToContainerPixel(new google.maps.LatLng(e[1],e[0]))}},{key:"latLngToContainerPoint",value:function(t){return this._projection.fromLatLngToContainerPixel(new google.maps.LatLng(t[1],t[0]))}}]),t}();e.default=r,t.exports=e.default},function(t,e,n){"use strict";function i(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var o=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}();e.Segment=function(){function t(e,n,o,r){i(this,t),this._start=e,this._end=n,this._length=o,this._lengthText=r}return o(t,[{key:"toJSON",value:function(){return{start_location:{lat:this._start[1],lng:this._start[0]},end_location:{lat:this._end[1],lng:this._end[0]},length:{text:this._lengthText,value:this._length}}}}]),t}()},function(t,e,n){"use strict";function i(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var o=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),r=n(1),a=n(80),s=function(t){return t&&t.__esModule?t:{default:t}}(a),u=function(){function t(e,n){i(this,t),this._context=n,this._overlay=e,this._overlay.onAdd=this.onAdd.bind(this),this._overlay.draw=this.draw.bind(this),this._overlay.onRemove=this.onRemove.bind(this),this.displayed=!1}return o(t,[{key:"onAdd",value:function(){this._overlay.getPanes().floatPane.appendChild(this.containerDiv)}},{key:"onRemove",value:function(){this.containerDiv.parentElement&&this.containerDiv.parentElement.removeChild(this.containerDiv)}},{key:"draw",value:function(){var t=this._overlay.getProjection().fromLatLngToDivPixel(this.position),e=Math.abs(t.x)<4e3&&Math.abs(t.y)<4e3?"block":"none";"block"===e&&(this.containerDiv.style.left=t.x+"px",this.containerDiv.style.top=t.y+"px"),this.containerDiv.style.display!==e&&(this.containerDiv.style.display=e)}},{key:"show",value:function(t,e){var n=this;this.position=t,this.cb=e,this.containerDiv=document.createElement("div"),this.containerDiv.classList.add(r.Config.prefix+"-single-context-menu"),this.containerDiv.id=r.Config.prefix+"-single-context-menu",this.containerDiv.stylesheet=s.default,this.containerDiv.oncontextmenu=function(t){return t.preventDefault()};var i=document.createElement("div");i.className=r.Config.prefix+"-single-context-menu-item",i.innerHTML='\n            <span class="'+r.Config.prefix+'-context-menu-item-delete-icon"></span>\n            <span class="'+r.Config.prefix+'-context-menu-item-text">Remove</span>\n        ',i.onclick=function(t){t.preventDefault(),e.apply(n._context),n.hide()},this.containerDiv.style.cssText="\n            display: block;\n        ",this.containerDiv.appendChild(i),this._overlay.setMap(this._context._map),this.displayed=!0,this._context&&this._context._map.panTo(this.position)}},{key:"hide",value:function(){this.displayed=!1,this._overlay&&this._overlay.setMap(null)}},{key:"isDisplayed",value:function(){return this.displayed}}]),t}();e.default=u,t.exports=e.default},function(t,e,n){"use strict";function i(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var o=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),r=n(1),a=n(81),s=function(t){return t&&t.__esModule?t:{default:t}}(a),u=function(){function t(e){i(this,t),this._parentDiv=e,this._containerDiv=document.createElement("div"),this._containerDiv.classList.add(r.Config.prefix+"-tooltip"),this._containerDiv.stylesheet=s.default,e.appendChild(this._containerDiv)}return o(t,[{key:"show",value:function(t,e){this._containerDiv.innerHTML=e,this._containerDiv.style.cssText="\n      display: block;\n      visibility: hidden;\n      position: absolute;\n    ";var n=this._parentDiv.getBoundingClientRect().width,i=this._parentDiv.getBoundingClientRect().height;this._containerDiv.style.cssText+=t.x<n/2?"left: "+t.x+"px;":"right: "+(n-t.x)+"px;",this._containerDiv.style.cssText+=t.y<i/2?"top: "+t.y+"px":"bottom: "+(i-t.y)+"px;",this._containerDiv.style.cssText+="visibility: visible;"}},{key:"hide",value:function(){this._containerDiv.style.cssText="display: none"}}]),t}();e.default=u,t.exports=e.default},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var i="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t};e.deepClone=function t(e){if(!e)return e;var n=[Number,String,Boolean],o=void 0;if(n.forEach(function(t){e instanceof t&&(o=t(e))}),void 0===o)if("[object Array]"===Object.prototype.toString.call(e))o=[],e.forEach(function(e,n,i){o[n]=t(e)});else if("object"===(void 0===e?"undefined":i(e)))if(e.nodeType&&"function"==typeof e.cloneNode)o=e.cloneNode(!0);else if(e.prototype)o=e;else if(e instanceof Date)o=new Date(e);else{o={};for(var r in e)o[r]=t(e[r])}else o=e;return o}},function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var i=n(42);n.d(e,"drag",function(){return i.a});var o=n(14);n.d(e,"dragDisable",function(){return o.a}),n.d(e,"dragEnable",function(){return o.b})},function(t,e,n){var i=n(36);"string"==typeof i&&(i=[[t.i,i,""]]);n(6)(i,{});i.locals&&(t.exports=i.locals)},function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{default:t}}function o(t){if(Array.isArray(t)){for(var e=0,n=Array(t.length);e<t.length;e++)n[e]=t[e];return n}return Array.from(t)}function r(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var a=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),s=n(33),u=(i(s),n(1)),l=n(22),c=i(l),h=n(29),f=i(h),p=n(30),d=i(p),g=n(2),v=n(27),_=i(v),m=n(24),y=n(28),A=n(32),x=n(25),b=i(x),w=n(13),C=n(23),B=n(26),E=n(31),T=function(){function t(e,n){r(this,t),t._initPolyfills(),this._options={showSegmentLength:!0,showAccumulativeLength:!0,contextMenu:!0,tooltip:!0,unit:w.UnitTypeId.METRIC,initialSegments:[]},Object.assign(this._options,n),this._map=e,this._map.setClickableIcons(!1),this._id=b.default.makeId(4),this._events=new Map,this._init()}return a(t,[{key:"lengthText",get:function(){return this._helper.formatLength(this._length||0)}},{key:"areaText",get:function(){return this._helper.formatArea(this._area||0)}},{key:"length",get:function(){return this._length||0}},{key:"area",get:function(){return this._area||0}},{key:"segments",get:function(){return(0,E.deepClone)(this._segments)||[]}},{key:"points",get:function(){return(0,E.deepClone)(this._geometry.nodes.map(function(t){return{lat:t[1],lng:t[0]}}))||[]}}],[{key:"UnitTypeId",get:function(){return w.UnitTypeId}}]),a(t,[{key:"_init",value:function(){this._containerDiv=this._map.getDiv().querySelector("div:first-child"),this._singleContextMenu=new f.default(new google.maps.OverlayView,this),this._options.contextMenu&&(this._contextMenu=new c.default(this._containerDiv,{width:160}),this._startElementNode=this._contextMenu.addItem("Measure distance",!0,this.start,this),this._endElementNode=this._contextMenu.addItem("Clear measurement",!1,this.end,this),this._bindToggleContextMenu()),this._options.tooltip&&(this._tooltip=new d.default(this._containerDiv)),this._helper=new b.default({unit:this._options.unit}),this._initOverlay()}},{key:"_bindToggleContextMenu",value:function(){var t=this;this._map.addListener("rightclick",function(e){t._firstClick=e,t._contextMenu.show(t._projection.fromLatLngToContainerPixel(e.latLng))}),document.addEventListener("keydown",function(e){27===e.which&&t._contextMenu.hide()}),this._containerDiv.addEventListener("mousedown",function(e){e.clientX>=t._contextMenu.left&&e.clientX<=t._contextMenu.left+t._contextMenu.width&&e.clientY>=t._contextMenu.top&&e.clientY<=t._contextMenu.top+t._contextMenu.height||t._contextMenu.hide()})}},{key:"start",value:function(t){var e=this;if(!this._started){this._overlay.setMap(this._map),this._geometry=new m.Geometry,this._segments=[];var n=t&&t.length>0;if(!this._options.contextMenu&&n)for(var i=0;i<t.length;i++){var o=t[i];if(this._geometry.addNode([o.lng,o.lat]),i>0){var r=t[i-1];this._updateSegment([r,o])}}this._options.contextMenu&&this._firstClick&&(this._checkClick(this._firstClick),this._contextMenu.toggleItems([this._endElementNode],[this._startElementNode])),this._mapClickEvent=this._map.addListener("click",function(t){return e._checkClick(t)}),this._map.setOptions({draggableCursor:"default"}),this._started=!0,"function"==typeof this._events.get(C.EVENT_START)&&this._events.get(C.EVENT_START)()}}},{key:"end",value:function(){this._started&&("function"==typeof this._events.get(C.EVENT_END)&&this._events.get(C.EVENT_END)(this._getResults()),this._options.contextMenu&&this._contextMenu.toggleItems([this._startElementNode],[this._endElementNode]),this._mapClickEvent.remove(),this._geometry=new m.Geometry,this._onRemoveOverlay(),this._setOverlay(),this._overlay.setMap(null),this._map.setOptions({draggableCursor:null}),this._started=!1)}},{key:"addListener",value:function(t,e){this._events.set(t,e)}},{key:"removeListener",value:function(t){this._events.delete(t)}},{key:"setOption",value:function(t,e){if(!this._options[t])throw new Error(t+" is not a valid option on MeasureTool");this._options[t]=e,this._helper._options[t]&&this._helper.setOption(t,e),this._overlay&&this._nodeCircles&&this._redrawOverlay()}},{key:"_initOverlay",value:function(){this._setOverlay(),this._initComplete=!1}},{key:"_setOverlay",value:function(){this._overlay=new google.maps.OverlayView,this._overlay.onAdd=this._onAddOverlay.bind(this),this._overlay.draw=this._onDrawOverlay.bind(this),this._overlay.onRemove=this._onRemoveOverlay.bind(this),this._overlay.setMap(this._map)}},{key:"_onAddOverlay",value:function(){this._initComplete||(this._initComplete=!0),this._projection=this._overlay.getProjection(),this._projectionUtility=new _.default(this._containerDiv,this._projection),this._svgOverlay=(0,g.select)(this._overlay.getPanes().overlayMouseTarget).append("div").attr("class",u.Config.prefix+"-measure-points-"+this._id).append("svg").attr("class",u.Config.prefix+"-svg-overlay"),this._linesBase=this._svgOverlay.append("g").attr("class","base"),this._linesBase.selectAll("line").data(this._geometry?this._geometry.lines:[]),this._linesAux=this._svgOverlay.append("g").attr("class","aux"),this._linesAux.selectAll("line").data(this._geometry?this._geometry.lines:[]),this._nodeCircles=this._svgOverlay.append("g").attr("class","node-circle"),this._nodeCircles.selectAll("circle").data(this._geometry?this._geometry.nodes:[]),this._options.showSegmentLength&&(this._segmentText=this._svgOverlay.append("g").attr("class","segment-text"),this._segmentText.selectAll("text").data(this._geometry?this._geometry.lines:[])),this._options.showAccumulativeLength&&(this._nodeText=this._svgOverlay.append("g").attr("class","node-text"),this._nodeText.selectAll("text").data(this._geometry?this._geometry.nodes:[])),this._hoverCircle=this._svgOverlay.append("g").attr("class","hover-circle"),this._hoverCircle.append("circle").attr("class","grey-circle").attr("r",8),this._initComplete&&!this._started&&this._overlay.setMap(null)}},{key:"_onDrawOverlay",value:function(){this._updateCircles(),this._updateLine(),this._options.showSegmentLength&&this._updateSegmentText(),this._options.showAccumulativeLength&&this._updateNodeText(),this._geometry&&this._updateArea(this._geometry.nodes.length-1,this._geometry.nodes[this._geometry.nodes.length-1]),this._dispatchMeasureEvent()}},{key:"_onRemoveOverlay",value:function(){(0,g.selectAll)("."+u.Config.prefix+"-measure-points-"+this._id).remove()}},{key:"_redrawOverlay",value:function(){this._onRemoveOverlay(),this._setOverlay(),this._overlay.draw()}},{key:"_checkClick",value:function(t){if(!this._clicked&&!this._dragged&&0==this._nodeCircles.selectAll('circle[r="9"]').size()&&!this._hoverCircle.select("circle").attr("cx")){if(!this._singleContextMenu||!this._singleContextMenu.isDisplayed()){var e=[t.latLng.lng(),t.latLng.lat()];this._geometry.addNode(e),this._overlay.draw()}this._singleContextMenu&&this._singleContextMenu.hide()}this._dragged=!1,this._clicked=!1,!0===/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)&&this._closed&&(this._singleContextMenu.hide(),this.ft=!1,this._closed=!1)}},{key:"_updateCircles",value:function(){var t=this,e=this;this._nodeCircles.selectAll("circle").data(this._geometry?this._geometry.nodes:[]).attr("class",function(t,e){return 0===e?"cover-circle head-circle":"cover-circle"}).attr("r",8).attr("cx",function(e){return t._projectionUtility.latLngToSvgPoint(e)[0]}).attr("cy",function(e){return t._projectionUtility.latLngToSvgPoint(e)[1]}).on("touchstart",function(t,n){e._onOverCircle(t,n,this)}).on("touchleave",function(t){e._onOutCircle(t,this)}).on("mousedown",function(){return t._hideTooltip()}).call(this._onDragCircle()).enter().append("circle").attr("class","cover-circle").attr("r",8).attr("cx",function(e){return t._projectionUtility.latLngToSvgPoint(e)[0]}).attr("cy",function(e){return t._projectionUtility.latLngToSvgPoint(e)[1]}).on("touchstart",function(t,n){e._onOverCircle(t,n,this)}).on("touchleave",function(t){e._onOutCircle(t,this)}).on("mousedown",function(){return t._hideTooltip()}).call(this._onDragCircle()),this._nodeCircles.selectAll(".removed-circle").remove()}},{key:"_openContextMenu",value:function(t,e,n){var i=this;this._singleContextMenu&&this._singleContextMenu.hide();var o=this;this._clicked=!0,this._singleContextMenu.show(new google.maps.LatLng(t[1],t[0]),function(){o._geometry.removeNode(e),(0,g.select)(n).classed("removed-circle",!0),o._overlay.draw(),!1===/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)&&(i._clicked=!0)})}},{key:"_updateLine",value:function(){var t=this;this._segments=[];var e=this._linesBase.selectAll("line").data(this._geometry?this._geometry.lines:[]).attr("class","base-line").attr("x1",function(e){return t._projectionUtility.latLngToSvgPoint(e[0])[0]}).attr("y1",function(e){return t._projectionUtility.latLngToSvgPoint(e[0])[1]}).attr("x2",function(e){return t._projectionUtility.latLngToSvgPoint(e[1])[0]}).attr("y2",function(e){return t._projectionUtility.latLngToSvgPoint(e[1])[1]}).each(function(e){return t._updateSegment(e)});e.enter().append("line").attr("class","base-line").attr("x1",function(e){return t._projectionUtility.latLngToSvgPoint(e[0])[0]}).attr("y1",function(e){return t._projectionUtility.latLngToSvgPoint(e[0])[1]}).attr("x2",function(e){return t._projectionUtility.latLngToSvgPoint(e[1])[0]}).attr("y2",function(e){return t._projectionUtility.latLngToSvgPoint(e[1])[1]}).each(function(e){return t._updateSegment(e)}),e.exit().remove();var n=this._linesAux.selectAll("line").data(this._geometry?this._geometry.lines:[]).attr("class","aux-line").attr("x1",function(e){return t._projectionUtility.latLngToSvgPoint(e[0])[0]}).attr("y1",function(e){return t._projectionUtility.latLngToSvgPoint(e[0])[1]}).attr("x2",function(e){return t._projectionUtility.latLngToSvgPoint(e[1])[0]}).attr("y2",function(e){return t._projectionUtility.latLngToSvgPoint(e[1])[1]}).on("mousemove",function(e){if(!1===/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){var n=b.default.findTouchPoint([t._projectionUtility.latLngToSvgPoint(e[0]),t._projectionUtility.latLngToSvgPoint(e[1])],[g.event.offsetX,g.event.offsetY]);t._updateHoverCirclePosition(n[0],n[1])}}).on("mouseout",function(e){return t._hideHoverCircle()}).on("mousedown",function(){return t._hideTooltip()}).call(this._onDragLine());n.enter().append("line").attr("class","aux-line").attr("x1",function(e){return t._projectionUtility.latLngToSvgPoint(e[0])[0]}).attr("y1",function(e){return t._projectionUtility.latLngToSvgPoint(e[0])[1]}).attr("x2",function(e){return t._projectionUtility.latLngToSvgPoint(e[1])[0]}).attr("y2",function(e){return t._projectionUtility.latLngToSvgPoint(e[1])[1]}).on("mousemove",function(e){if(!1===/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){var n=b.default.findTouchPoint([t._projectionUtility.latLngToSvgPoint(e[0]),t._projectionUtility.latLngToSvgPoint(e[1])],[g.event.offsetX,g.event.offsetY]);t._updateHoverCirclePosition(n[0],n[1])}}).on("mouseout",function(e){return t._hideHoverCircle()}).on("mousedown",function(){return t._hideTooltip()}).call(this._onDragLine()),n.exit().remove()}},{key:"_updateSegmentText",value:function(){var t=this,e=this._segmentText.selectAll("text").data(this._geometry?this._geometry.lines:[]).attr("class","segment-measure-text").attr("text-anchor","middle").attr("dominant-baseline","text-before-edge").attr("transform",function(e){var n=t._projectionUtility.latLngToSvgPoint(e[0]),i=t._projectionUtility.latLngToSvgPoint(e[1]);return b.default.transformText(n,i)}).text(function(e,n){return t._helper.formatLength(t._helper.computeLengthBetween(e[0],e[1]))});e.enter().append("text").attr("class","segment-measure-text").attr("text-anchor","middle").attr("dominant-baseline","text-before-edge").attr("transform",function(e){var n=t._projectionUtility.latLngToSvgPoint(e[0]),i=t._projectionUtility.latLngToSvgPoint(e[1]);return b.default.transformText(n,i)}).text(function(e,n){return t._helper.formatLength(t._helper.computeLengthBetween(e[0],e[1]))}),e.exit().remove()}},{key:"_updateNodeText",value:function(){var t=this,e=this._nodeText.selectAll("text").data(this._geometry?this._geometry.nodes:[]).attr("class",function(t,e){return 0===e?"node-measure-text head-text":"node-measure-text"}).attr("text-anchor","middle").attr("dominant-baseline","text-after-edge").attr("x",function(e){return t._projectionUtility.latLngToSvgPoint(e)[0]}).attr("y",this._transformNodeTextY.bind(this)).text(function(e,n){var i=t._helper.computePathLength(t._geometry.nodes.slice(0,n+1));return n===t._geometry.nodes.length-1&&(t._length=i),t._helper.formatLength(i)});e.enter().append("text").attr("class",function(t,e){return 0===e?"node-measure-text head-text":"node-measure-text"}).attr("text-anchor","middle").attr("dominant-baseline","text-after-edge").attr("x",function(e){return t._projectionUtility.latLngToSvgPoint(e)[0]}).attr("y",this._transformNodeTextY.bind(this)).text(function(e,n){var i=t._helper.computePathLength(t._geometry.nodes.slice(0,n+1));return n===t._geometry.nodes.length-1&&(t._length=i),t._helper.formatLength(i)}),e.exit().remove()}},{key:"_onOverCircle",value:function(t,e,n){this._dragging||((0,g.select)(n).attr("r",9),this._options.tooltip&&this._tooltip.show(this._projectionUtility.latLngToContainerPoint(t),0===e?u.Config.tooltipText2:u.Config.tooltipText1))}},{key:"_onOutCircle",value:function(t,e){(0,g.select)(e).attr("r",8),this._hideTooltip()}},{key:"_onDragCircle",value:function(){var t=this,e=!1,n=(0,A.drag)().on("drag",function(n,i){e=!0,t._dragging=!0,(0,g.select)(this).attr("cx",g.event.x).attr("cy",g.event.y),t._updateLinePosition.call(t,t._linesBase,i),t._updateLinePosition.call(t,t._linesAux,i),t._options.showSegmentLength&&t._updateSegmentTextPosition(i),t._options.showAccumulativeLength&&t._updateNodeTextPosition(i),t._updateArea(i,t._projectionUtility.svgPointToLatLng([g.event.x,g.event.y]))});return n.on("start",function(e,n){g.event.sourceEvent.stopPropagation(),(0,g.select)(this).raise().attr("r",9),t._disableMapScroll()}),n.on("end",function(n,i){t._enableMapScroll(),e?(t._geometry.updateNode(i,t._projectionUtility.svgPointToLatLng([g.event.x,g.event.y])),t._showTooltipOnEvent(0===i?u.Config.tooltipText2:u.Config.tooltipText1)):i>0?t._openContextMenu(n,i,this):!1===/Android|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)&&(t._geometry.addNode(n),t._dragged=!0),e=!1,t._dragging=!1,t._overlay.draw()}),n}},{key:"_onDragLine",value:function(){var t=this,e=!1,n=(0,A.drag)();return!1===/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)&&(n.on("drag",function(n,i){t._dragging=!0,e||(e=!0,t._geometry.insertNode(i+1,t._projectionUtility.svgPointToLatLng([g.event.x,g.event.y])),t._updateLine(),t._options.showSegmentLength&&t._updateSegmentText(),t._options.showAccumulativeLength&&t._updateNodeText()),t._updateHoverCirclePosition(g.event.x,g.event.y),t._updateLinePosition(t._linesBase,i+1),t._updateLinePosition(t._linesAux,i+1),t._options.showSegmentLength&&t._updateSegmentTextPosition(i+1),t._options.showAccumulativeLength&&t._updateNodeTextPosition(i+1),t._updateArea(i+1,t._projectionUtility.svgPointToLatLng([g.event.x,g.event.y]))}),n.on("start",function(){g.event.sourceEvent.stopPropagation(),t._hoverCircle.select("circle").attr("class","cover-circle"),t._disableMapScroll()}),n.on("end",function(n,i){t._enableMapScroll(),e&&(t._geometry.updateNode(i+1,t._projectionUtility.svgPointToLatLng([g.event.x,g.event.y])),t._hideHoverCircle(),t._overlay.draw(),e=!1,t._showTooltipOnEvent(u.Config.tooltipText1)),t._updateArea(i+1,t._projectionUtility.svgPointToLatLng([g.event.x,g.event.y])),t._hoverCircle.select("circle").attr("class","grey-circle"),t._dragging=!1})),n}},{key:"_updateLinePosition",value:function(t,e){e<this._geometry.lines.length&&t.select("line:nth-child("+(e+1)+")").attr("x1",g.event.x).attr("y1",g.event.y),e>0&&t.select("line:nth-child("+e+")").attr("x2",g.event.x).attr("y2",g.event.y)}},{key:"_updateSegmentTextPosition",value:function(t){var e=this;t<this._geometry.lines.length&&this._segmentText.select("text:nth-child("+(t+1)+")").attr("transform",function(t){var n=[g.event.x,g.event.y],i=e._projectionUtility.latLngToSvgPoint(t[1]);return b.default.transformText(n,i)}).text(function(t){return e._helper.formatLength(e._helper.computeLengthBetween(e._projectionUtility.svgPointToLatLng([g.event.x,g.event.y]),t[1]))}),t>0&&this._segmentText.select("text:nth-child("+t+")").attr("transform",function(t){var n=e._projectionUtility.latLngToSvgPoint(t[0]),i=[g.event.x,g.event.y];return b.default.transformText(n,i)}).text(function(t){return e._helper.formatLength(e._helper.computeLengthBetween(t[0],e._projectionUtility.svgPointToLatLng([g.event.x,g.event.y])))})}},{key:"_updateNodeTextPosition",value:function(t){var e=this;this._nodeText.select("text:nth-child("+(t+1)+")").attr("x",g.event.x).attr("y",function(){var n=void 0;return n=t>0&&e._projectionUtility.latLngToSvgPoint(e._geometry.nodes[t-1])[1]<g.event.y?23:-7,g.event.y+n}),this._nodeText.select("text:nth-child("+(t+2)+")").attr("y",function(n){var i=void 0;return i=t+1>0&&g.event.y<e._projectionUtility.latLngToSvgPoint(n)[1]?23:-7,e._projectionUtility.latLngToSvgPoint(n)[1]+i}),this._nodeText.selectAll("text").filter(function(e,n){return n>=t}).text(function(n,i){var r=e._helper.computePathLength([].concat(o(e._geometry.nodes.slice(0,t)),[e._projectionUtility.svgPointToLatLng([g.event.x,g.event.y])],o(e._geometry.nodes.slice(t+1,t+1+i))));return t+i===e._geometry.nodes.length-1&&(e._length=r),e._helper.formatLength(r)})}},{key:"_updateHoverCirclePosition",value:function(t,e){this._hoverCircle.select("circle").attr("cx",t).attr("cy",e),this._dragging||this._options.tooltip&&this._tooltip.show(this._projectionUtility.svgPointToContainerPoint([t,e]),u.Config.tooltipText2)}},{key:"_hideHoverCircle",value:function(){this._hoverCircle.select("circle").attr("cx",null).attr("cy",null),this._hideTooltip()}},{key:"_disableMapScroll",value:function(){this._map.setOptions({scrollwheel:!1,gestureHandling:"none"})}},{key:"_enableMapScroll",value:function(){this._map.setOptions({scrollwheel:!0,gestureHandling:"auto"})}},{key:"_transformNodeTextY",value:function(t,e){var n=void 0;return n=e>0&&this._geometry.nodes[e-1][1]>t[1]?23:-7,this._projectionUtility.latLngToSvgPoint(t)[1]+n}},{key:"_updateArea",value:function(t,e){if(this._geometry){var n=this._geometry.nodes.length,i=1/80*this.length,r=void 0,a=0;n>2&&(0===t?(r=this._helper.computeLengthBetween(this._geometry.nodes[n-1],e),a=r>i?0:this._helper.computeArea([e].concat(o(this._geometry.nodes.slice(1,n-1))))):t===n-1?(r=this._helper.computeLengthBetween(e,this._geometry.nodes[0]),a=r>i?0:this._helper.computeArea(this._geometry.nodes.slice(0,n-1))):t>0&&t<n-1?(r=this._helper.computeLengthBetween(this._geometry.nodes[0],this._geometry.nodes[n-1]),a=r>i?0:this._helper.computeArea([].concat(o(this._geometry.nodes.slice(0,t)),[e],o(this._geometry.nodes.slice(t+1))))):(r=this._helper.computeLengthBetween(this._geometry.nodes[0],this._geometry.nodes[n-1]),a=r>i?0:this._helper.computeArea(this._geometry.nodes))),this._area=a,a>0?(this.ft&&(this._closed=!0),this._nodeText.select(":last-child").text("Total distance: "+this.lengthText+"; Total area: "+this.areaText+".")):this.ft=!0}}},{key:"_showTooltipOnEvent",value:function(t){this._options.tooltip&&this._tooltip.show(this._projectionUtility.svgPointToContainerPoint([g.event.x,g.event.y]),t)}},{key:"_hideTooltip",value:function(){this._options.tooltip&&this._tooltip.hide()}},{key:"_dispatchMeasureEvent",value:function(){if(this._started){var t=this._getResults();this._lastMeasure&&this._lastMeasure.result.lengthText===this.lengthText&&this._lastMeasure.result.areaText===this.areaText||("function"==typeof this._events.get(C.EVENT_CHANGE)&&this._events.get(C.EVENT_CHANGE)(this._lastMeasure=t),this._overlay.draw())}}},{key:"_updateSegment",value:function(t){var e=this._helper.computeLengthBetween(t[0],t[1]),n=this._helper.formatLength(e);this._segments.push(new y.Segment(t[0],t[1],e,n).toJSON())}},{key:"_getResults",value:function(){return{result:{length:this.length,lengthText:this.lengthText,area:this.area,areaText:this.areaText,segments:this.segments,points:this.points}}}}],[{key:"_initPolyfills",value:function(){(0,B.ObjectAssign)()}}]),t}();e.default=T,t.exports=e.default},function(t,e,n){e=t.exports=n(3)(),e.push([t.i,".measure-tool-context-menu {\n  font-family: Roboto, Arial, sans-serif;\n  border: 1px solid rgba(0, 0, 0, 0.2);\n  outline: none;\n  position: fixed;\n  display: none;\n  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);\n  transform: translate3d(0, 0, 0);\n  max-width: 265px;\n  z-index: 1;\n  outline-offset: -2px;\n  background: #fff;\n  padding: 6px 0;\n  white-space: nowrap;\n  cursor: default;\n  margin: 0; }\n  .measure-tool-context-menu ul {\n    padding: 0px;\n    margin: 0px;\n    background-color: white; }\n    .measure-tool-context-menu ul li {\n      cursor: pointer;\n      display: flex;\n      align-items: center;\n      border-color: transparent;\n      border-style: dotted;\n      border-width: 1px 0;\n      color: #333;\n      font-size: 13px;\n      font-weight: normal;\n      margin: 0;\n      padding: 4px 44px 4px 16px;\n      position: relative;\n      white-space: nowrap; }\n      .measure-tool-context-menu ul li:hover {\n        background-color: #f1f1f1;\n        border-color: #f1f1f1;\n        color: #222;\n        transition: background 0s; }\n","",{version:3,sources:["/Volumes/Data/Projects/MeasureTool-GoogleMaps-V3/src/context-menu.scss"],names:[],mappings:"AACA;EACE,uCAFqC;EAGrC,qCAAiC;EACjC,cAAa;EACb,gBAAe;EACf,cAAa;EAEb,yCAAqC;EACrC,gCAA6B;EAC7B,iBAAgB;EAChB,WAAU;EACV,qBAAoB;EAEpB,iBAAgB;EAChB,eAAc;EACd,oBAAmB;EACnB,gBAAe;EACf,UAAS,EA6BV;EA9CD;IAmBI,aAAY;IACZ,YAAW;IACX,wBAAuB,EAwBxB;IA7CH;MAwBM,gBAAe;MACf,cAAa;MACb,oBAAmB;MACnB,0BAAyB;MACzB,qBAAoB;MACpB,oBAAmB;MACnB,YAAW;MACX,gBAAe;MACf,oBAAmB;MACnB,UAAS;MACT,2BAA0B;MAC1B,mBAAkB;MAClB,oBAAmB,EAQpB;MA5CL;QAuCQ,0BAAyB;QACzB,sBAAqB;QACrB,YAAW;QACX,0BAAyB,EAC1B",file:"context-menu.scss",sourcesContent:["$font-family: Roboto, Arial, sans-serif;\n.measure-tool-context-menu {\n  font-family: $font-family;\n  border: 1px solid rgba(0,0,0,0.2);\n  outline: none;\n  position: fixed;\n  display: none;\n\n  box-shadow: 0 2px 2px rgba(0,0,0,0.2);\n  transform: translate3d(0,0,0);\n  max-width: 265px;\n  z-index: 1;\n  outline-offset: -2px;\n\n  background: #fff;\n  padding: 6px 0;\n  white-space: nowrap;\n  cursor: default;\n  margin: 0;\n  ul {\n    padding: 0px;\n    margin: 0px;\n    background-color: white;\n\n    li {\n      cursor: pointer;\n      display: flex;\n      align-items: center;\n      border-color: transparent;\n      border-style: dotted;\n      border-width: 1px 0;\n      color: #333;\n      font-size: 13px;\n      font-weight: normal;\n      margin: 0;\n      padding: 4px 44px 4px 16px;\n      position: relative;\n      white-space: nowrap;\n\n      &:hover {\n        background-color: #f1f1f1;\n        border-color: #f1f1f1;\n        color: #222;\n        transition: background 0s;\n      }\n    }\n  }\n}"],sourceRoot:""}])},function(t,e,n){e=t.exports=n(3)(),e.push([t.i,".measure-tool-svg-overlay {\n  position: absolute;\n  top: -4000px;\n  left: -4000px;\n  width: 8000px;\n  height: 8000px; }\n\n.dragging-circle,\n.cover-circle {\n  fill: white;\n  stroke: #2196F3;\n  stroke-width: 4px; }\n  .dragging-circle:hover,\n  .cover-circle:hover {\n    cursor: pointer; }\n\n.grey-circle {\n  fill: #fcfcfc;\n  stroke: #646464;\n  stroke-width: 4px;\n  pointer-events: none; }\n\n.base-line {\n  fill: none;\n  stroke: #2196F3;\n  stroke-width: 4px; }\n\n.aux-line {\n  fill: none;\n  stroke: transparent;\n  stroke-width: 8px;\n  cursor: pointer; }\n\n.segment-measure-text {\n  stroke: black;\n  pointer-events: none; }\n\n.node-measure-text {\n  text-shadow: -1.4px -1.4px rgba(255, 255, 255, 0.4), -1.4px 1.4px rgba(255, 255, 255, 0.4), 1.4px 1.4px rgba(255, 255, 255, 0.4), 1.4px -1.4px rgba(255, 255, 255, 0.4), -1.4px 0 rgba(255, 255, 255, 0.4), 0 1.4px rgba(255, 255, 255, 0.4), 1.4px 0 rgba(255, 255, 255, 0.4), 0 -1.4px rgba(255, 255, 255, 0.4);\n  pointer-events: none; }\n  .node-measure-text.head-text {\n    visibility: hidden; }\n","",{version:3,sources:["/Volumes/Data/Projects/MeasureTool-GoogleMaps-V3/src/index.scss"],names:[],mappings:"AAEA;EACE,mBAAkB;EAClB,aAAY;EACZ,cAAa;EACb,cAAa;EACb,eAAc,EACf;;AAED;;EAEE,YAAW;EACX,gBAAe;EACf,kBAAiB,EAIlB;EARD;;IAMI,gBAAe,EAChB;;AAGH;EACE,cAAwB;EACxB,gBAA0B;EAC1B,kBAAiB;EACjB,qBAAoB,EAIrB;;AAED;EACE,WAAU;EACV,gBAAe;EACf,kBAAiB,EAClB;;AAED;EACE,WAAU;EACV,oBAAmB;EACnB,kBAAiB;EACjB,gBAAe,EAChB;;AAED;EACE,cAAa;EACb,qBAAoB,EACrB;;AAED;EACE,kTAjDqC;EAyDrC,qBAAoB,EAKrB;EAdD;IAYI,mBAAkB,EACnB",file:"index.scss",sourcesContent:["$opaque-white: rgba(255, 255, 255, 0.4);\n\n.measure-tool-svg-overlay{\n  position: absolute;\n  top: -4000px;\n  left: -4000px;\n  width: 8000px;\n  height: 8000px;\n}\n\n.dragging-circle,\n.cover-circle {\n  fill: white;\n  stroke: #2196F3;\n  stroke-width: 4px;\n  &:hover {\n    cursor: pointer;\n  }\n}\n\n.grey-circle {\n  fill: rgb(252, 252, 252);\n  stroke: rgb(100, 100, 100);\n  stroke-width: 4px;\n  pointer-events: none;\n  //&:hover {\n  //  cursor: pointer;\n  //}\n}\n\n.base-line {\n  fill: none;\n  stroke: #2196F3;\n  stroke-width: 4px;\n}\n\n.aux-line {\n  fill: none;\n  stroke: transparent;\n  stroke-width: 8px;\n  cursor: pointer;\n}\n\n.segment-measure-text {\n  stroke: black;\n  pointer-events: none;\n}\n\n.node-measure-text {\n  text-shadow: -1.4px -1.4px $opaque-white,\n               -1.4px 1.4px $opaque-white,\n                1.4px 1.4px $opaque-white,\n                1.4px -1.4px $opaque-white,\n                -1.4px 0 $opaque-white,\n                0 1.4px $opaque-white,\n                1.4px 0 $opaque-white,\n                0 -1.4px $opaque-white;\n  pointer-events: none;\n\n  &.head-text {\n    visibility: hidden;\n  }\n}\n"],sourceRoot:""}])},function(t,e,n){e=t.exports=n(3)(),e.push([t.i,'.measure-tool-single-context-menu {\n  outline: none;\n  position: fixed;\n  display: none;\n  transform: translate3d(0, 0, 0);\n  max-width: 265px;\n  z-index: 99999999;\n  outline-offset: -2px;\n  white-space: nowrap;\n  cursor: default;\n  margin: 0;\n  background: #FFFFFF;\n  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.48);\n  border-radius: 3px;\n  padding: 8px 0px; }\n\n.measure-tool-single-context-menu-item {\n  padding: 10px 0px;\n  cursor: pointer;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  align-content: space-between; }\n\n.measure-tool-context-menu-item-delete-icon {\n  display: inline-block;\n  margin: 0px 26px 0px 30px;\n  width: 18px;\n  height: 18px;\n  color: rgba(0, 0, 0, 0.54);\n  background: url(\'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="grey" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"/></svg>\'); }\n\n.measure-tool-context-menu-item-text {\n  font-family: Titillium Web;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 28px;\n  letter-spacing: 0.44px;\n  color: #000000;\n  padding-right: 40px; }\n',"",{version:3,sources:["/Volumes/Data/Projects/MeasureTool-GoogleMaps-V3/src/single-context-menu.scss"],names:[],mappings:"AAAA;EACI,cAAa;EACb,gBAAe;EACf,cAAa;EACb,gCAA6B;EAC7B,iBAAgB;EAChB,kBAAiB;EACjB,qBAAoB;EACpB,oBAAmB;EACnB,gBAAe;EACf,UAAS;EACT,oBAAmB;EACnB,6CAA4C;EAC5C,mBAAkB;EAClB,iBAAgB,EACnB;;AAED;EACI,kBAAiB;EACjB,gBAAe;EACf,cAAa;EACb,oBAAmB;EACnB,oBAAmB;EACnB,6BAA4B,EAC/B;;AAED;EACI,sBAAqB;EACrB,0BAAyB;EACzB,YAAW;EACX,aAAY;EACZ,2BAA0B;EAC1B,+RAA8R,EACjS;;AAED;EACI,2BAA0B;EAC1B,mBAAkB;EAClB,oBAAmB;EACnB,gBAAe;EACf,kBAAiB;EACjB,uBAAsB;EACtB,eAAc;EACd,oBAAmB,EACtB",file:"single-context-menu.scss",sourcesContent:['.measure-tool-single-context-menu {\n    outline: none;\n    position: fixed;\n    display: none;\n    transform: translate3d(0,0,0);\n    max-width: 265px;\n    z-index: 99999999;\n    outline-offset: -2px;\n    white-space: nowrap;\n    cursor: default;\n    margin: 0;\n    background: #FFFFFF;\n    box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.48);\n    border-radius: 3px;\n    padding: 8px 0px;\n}\n\n.measure-tool-single-context-menu-item {\n    padding: 10px 0px;\n    cursor: pointer;\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    align-content: space-between;\n}\n\n.measure-tool-context-menu-item-delete-icon {\n    display: inline-block;\n    margin: 0px 26px 0px 30px;\n    width: 18px;\n    height: 18px;\n    color: rgba(0, 0, 0, 0.54);\n    background: url(\'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="grey" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"/></svg>\');\n}\n\n.measure-tool-context-menu-item-text {\n    font-family: Titillium Web;\n    font-style: normal;\n    font-weight: normal;\n    font-size: 16px;\n    line-height: 28px;\n    letter-spacing: 0.44px;\n    color: #000000;\n    padding-right: 40px;\n}'],sourceRoot:""}])},function(t,e,n){e=t.exports=n(3)(),e.push([t.i,".measure-tool-tooltip {\n  display: none;\n  font-family: Roboto, Arial, sans-serif;\n  margin: 6px 15px;\n  background-color: #fff;\n  border-radius: 2px;\n  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3);\n  padding: 10px;\n  overflow: hidden;\n  pointer-events: none;\n  font-size: 0.7rem;\n  z-index: 999; }\n","",{version:3,sources:["/Volumes/Data/Projects/MeasureTool-GoogleMaps-V3/src/tooltip.scss"],names:[],mappings:"AACA;EACE,cAAa;EACb,uCAHqC;EAIrC,iBAAgB;EAChB,uBAAsB;EACtB,mBAAkB;EAClB,yCAAqC;EACrC,cAAa;EACb,iBAAgB;EAChB,qBAAoB;EACpB,kBAAiB;EACjB,aAAY,EACb",file:"tooltip.scss",sourcesContent:["$font-family: Roboto, Arial, sans-serif;\n.measure-tool-tooltip {\n  display: none;\n  font-family: $font-family;\n  margin: 6px 15px;\n  background-color: #fff;\n  border-radius: 2px;\n  box-shadow: 0 1px 4px rgba(0,0,0,0.3);\n  padding: 10px;\n  overflow: hidden;\n  pointer-events: none;\n  font-size: 0.7rem;\n  z-index: 999;\n}"],sourceRoot:""}])},function(t,e,n){"use strict";function i(){for(var t,e=0,n=arguments.length,i={};e<n;++e){if(!(t=arguments[e]+"")||t in i||/[\s.]/.test(t))throw new Error("illegal type: "+t);i[t]=[]}return new o(i)}function o(t){this._=t}function r(t,e){return t.trim().split(/^|\s+/).map(function(t){var n="",i=t.indexOf(".");if(i>=0&&(n=t.slice(i+1),t=t.slice(0,i)),t&&!e.hasOwnProperty(t))throw new Error("unknown type: "+t);return{type:t,name:n}})}function a(t,e){for(var n,i=0,o=t.length;i<o;++i)if((n=t[i]).name===e)return n.value}function s(t,e,n){for(var i=0,o=t.length;i<o;++i)if(t[i].name===e){t[i]=u,t=t.slice(0,i).concat(t.slice(i+1));break}return null!=n&&t.push({name:e,value:n}),t}var u={value:function(){}};o.prototype=i.prototype={constructor:o,on:function(t,e){var n,i=this._,o=r(t+"",i),u=-1,l=o.length;{if(!(arguments.length<2)){if(null!=e&&"function"!=typeof e)throw new Error("invalid callback: "+e);for(;++u<l;)if(n=(t=o[u]).type)i[n]=s(i[n],t.name,e);else if(null==e)for(n in i)i[n]=s(i[n],t.name,null);return this}for(;++u<l;)if((n=(t=o[u]).type)&&(n=a(i[n],t.name)))return n}},copy:function(){var t={},e=this._;for(var n in e)t[n]=e[n].slice();return new o(t)},call:function(t,e){if((n=arguments.length-2)>0)for(var n,i,o=new Array(n),r=0;r<n;++r)o[r]=arguments[r+2];if(!this._.hasOwnProperty(t))throw new Error("unknown type: "+t);for(i=this._[t],r=0,n=i.length;r<n;++r)i[r].value.apply(e,o)},apply:function(t,e,n){if(!this._.hasOwnProperty(t))throw new Error("unknown type: "+t);for(var i=this._[t],o=0,r=i.length;o<r;++o)i[o].value.apply(e,n)}},e.a=i},function(t,e,n){"use strict";var i=n(39);n.d(e,"a",function(){return i.a})},function(t,e,n){"use strict";e.a=function(t){return function(){return t}}},function(t,e,n){"use strict";function i(){return!u.event.ctrlKey&&!u.event.button}function o(){return this.parentNode}function r(t){return null==t?{x:u.event.x,y:u.event.y}:t}function a(){return navigator.maxTouchPoints||"ontouchstart"in this}var s=n(40),u=n(2),l=n(14),c=n(15),h=n(41),f=n(43);e.a=function(){function t(t){t.on("mousedown.drag",e).filter(E).on("touchstart.drag",g).on("touchmove.drag",v).on("touchend.drag touchcancel.drag",_).style("touch-action","none").style("-webkit-tap-highlight-color","rgba(0,0,0,0)")}function e(){if(!b&&w.apply(this,arguments)){var t=m("mouse",C.apply(this,arguments),u.mouse,this,arguments);t&&(n.i(u.select)(u.event.view).on("mousemove.drag",p,!0).on("mouseup.drag",d,!0),n.i(l.a)(u.event.view),n.i(c.b)(),x=!1,y=u.event.clientX,A=u.event.clientY,t("start"))}}function p(){if(n.i(c.a)(),!x){var t=u.event.clientX-y,e=u.event.clientY-A;x=t*t+e*e>M}T.mouse("drag")}function d(){n.i(u.select)(u.event.view).on("mousemove.drag mouseup.drag",null),n.i(l.b)(u.event.view,x),n.i(c.a)(),T.mouse("end")}function g(){if(w.apply(this,arguments)){var t,e,i=u.event.changedTouches,o=C.apply(this,arguments),r=i.length;for(t=0;t<r;++t)(e=m(i[t].identifier,o,u.touch,this,arguments))&&(n.i(c.b)(),e("start"))}}function v(){var t,e,i=u.event.changedTouches,o=i.length;for(t=0;t<o;++t)(e=T[i[t].identifier])&&(n.i(c.a)(),e("drag"))}function _(){var t,e,i=u.event.changedTouches,o=i.length;for(b&&clearTimeout(b),b=setTimeout(function(){b=null},500),t=0;t<o;++t)(e=T[i[t].identifier])&&(n.i(c.b)(),e("end"))}function m(e,i,o,r,a){var s,l,c,h=o(i,e),p=k.copy();if(n.i(u.customEvent)(new f.a(t,"beforestart",s,e,L,h[0],h[1],0,0,p),function(){return null!=(u.event.subject=s=B.apply(r,a))&&(l=s.x-h[0]||0,c=s.y-h[1]||0,!0)}))return function d(g){var v,_=h;switch(g){case"start":T[e]=d,v=L++;break;case"end":delete T[e],--L;case"drag":h=o(i,e),v=L}n.i(u.customEvent)(new f.a(t,g,s,e,v,h[0]+l,h[1]+c,h[0]-_[0],h[1]-_[1],p),p.apply,p,[g,r,a])}}var y,A,x,b,w=i,C=o,B=r,E=a,T={},k=n.i(s.a)("start","drag","end"),L=0,M=0;return t.filter=function(e){return arguments.length?(w="function"==typeof e?e:n.i(h.a)(!!e),t):w},t.container=function(e){return arguments.length?(C="function"==typeof e?e:n.i(h.a)(e),t):C},t.subject=function(e){return arguments.length?(B="function"==typeof e?e:n.i(h.a)(e),t):B},t.touchable=function(e){return arguments.length?(E="function"==typeof e?e:n.i(h.a)(!!e),t):E},t.on=function(){var e=k.on.apply(k,arguments);return e===k?t:e},t.clickDistance=function(e){return arguments.length?(M=(e=+e)*e,t):Math.sqrt(M)},t}},function(t,e,n){"use strict";function i(t,e,n,i,o,r,a,s,u,l){this.target=t,this.type=e,this.subject=n,this.identifier=i,this.active=o,this.x=r,this.y=a,this.dx=s,this.dy=u,this._=l}e.a=i,i.prototype.on=function(){var t=this._.on.apply(this._,arguments);return t===this._?this:t}},function(t,e,n){"use strict";e.a=function(t){return function(){return t}}},function(t,e,n){"use strict";var i=n(4),o=n(17);e.a=function(t){return n.i(o.a)(n.i(i.a)(t).call(document.documentElement))}},function(t,e,n){"use strict";function i(){return new o}function o(){this._="@"+(++r).toString(36)}e.a=i;var r=0;o.prototype=i.prototype={constructor:o,get:function(t){for(var e=this._;!(e in t);)if(!(t=t.parentNode))return;return t[e]},set:function(t,e){return t[this._]=e},remove:function(t){return this._ in t&&delete t[this._]},toString:function(){return this._}}},function(t,e,n){"use strict";var i=n(11),o=n(5);e.a=function(t){var e=n.i(i.a)();return e.changedTouches&&(e=e.changedTouches[0]),n.i(o.a)(t,e)}},function(t,e,n){"use strict";var i=n(0);e.a=function(t){return"string"==typeof t?new i.b([document.querySelectorAll(t)],[document.documentElement]):new i.b([null==t?[]:t],i.c)}},function(t,e,n){"use strict";var i=n(4);e.a=function(t){var e="function"==typeof t?t:n.i(i.a)(t);return this.select(function(){return this.appendChild(e.apply(this,arguments))})}},function(t,e,n){"use strict";function i(t){return function(){this.removeAttribute(t)}}function o(t){return function(){this.removeAttributeNS(t.space,t.local)}}function r(t,e){return function(){this.setAttribute(t,e)}}function a(t,e){return function(){this.setAttributeNS(t.space,t.local,e)}}function s(t,e){return function(){var n=e.apply(this,arguments);null==n?this.removeAttribute(t):this.setAttribute(t,n)}}function u(t,e){return function(){var n=e.apply(this,arguments);null==n?this.removeAttributeNS(t.space,t.local):this.setAttributeNS(t.space,t.local,n)}}var l=n(7);e.a=function(t,e){var c=n.i(l.a)(t);if(arguments.length<2){var h=this.node();return c.local?h.getAttributeNS(c.space,c.local):h.getAttribute(c)}return this.each((null==e?c.local?o:i:"function"==typeof e?c.local?u:s:c.local?a:r)(c,e))}},function(t,e,n){"use strict";e.a=function(){var t=arguments[0];return arguments[0]=this,t.apply(null,arguments),this}},function(t,e,n){"use strict";function i(t){return t.trim().split(/^|\s+/)}function o(t){return t.classList||new r(t)}function r(t){this._node=t,this._names=i(t.getAttribute("class")||"")}function a(t,e){for(var n=o(t),i=-1,r=e.length;++i<r;)n.add(e[i])}function s(t,e){for(var n=o(t),i=-1,r=e.length;++i<r;)n.remove(e[i])}function u(t){return function(){a(this,t)}}function l(t){return function(){s(this,t)}}function c(t,e){return function(){(e.apply(this,arguments)?a:s)(this,t)}}r.prototype={add:function(t){this._names.indexOf(t)<0&&(this._names.push(t),this._node.setAttribute("class",this._names.join(" ")))},remove:function(t){var e=this._names.indexOf(t);e>=0&&(this._names.splice(e,1),this._node.setAttribute("class",this._names.join(" ")))},contains:function(t){return this._names.indexOf(t)>=0}},e.a=function(t,e){var n=i(t+"");if(arguments.length<2){for(var r=o(this.node()),a=-1,s=n.length;++a<s;)if(!r.contains(n[a]))return!1;return!0}return this.each(("function"==typeof e?c:e?u:l)(n,e))}},function(t,e,n){"use strict";function i(){var t=this.cloneNode(!1),e=this.parentNode;return e?e.insertBefore(t,this.nextSibling):t}function o(){var t=this.cloneNode(!0),e=this.parentNode;return e?e.insertBefore(t,this.nextSibling):t}e.a=function(t){return this.select(t?o:i)}},function(t,e,n){"use strict";function i(t,e,n,i,o,r){for(var s,u=0,l=e.length,c=r.length;u<c;++u)(s=e[u])?(s.__data__=r[u],i[u]=s):n[u]=new a.b(t,r[u]);for(;u<l;++u)(s=e[u])&&(o[u]=s)}function o(t,e,n,i,o,r,s){var l,c,h,f={},p=e.length,d=r.length,g=new Array(p);for(l=0;l<p;++l)(c=e[l])&&(g[l]=h=u+s.call(c,c.__data__,l,e),h in f?o[l]=c:f[h]=c);for(l=0;l<d;++l)h=u+s.call(t,r[l],l,r),(c=f[h])?(i[l]=c,c.__data__=r[l],f[h]=null):n[l]=new a.b(t,r[l]);for(l=0;l<p;++l)(c=e[l])&&f[g[l]]===c&&(o[l]=c)}var r=n(0),a=n(18),s=n(44),u="$";e.a=function(t,e){if(!t)return m=new Array(this.size()),d=-1,this.each(function(t){m[++d]=t}),m;var a=e?o:i,u=this._parents,l=this._groups;"function"!=typeof t&&(t=n.i(s.a)(t));for(var c=l.length,h=new Array(c),f=new Array(c),p=new Array(c),d=0;d<c;++d){var g=u[d],v=l[d],_=v.length,m=t.call(g,g&&g.__data__,d,u),y=m.length,A=f[d]=new Array(y),x=h[d]=new Array(y);a(g,v,A,x,p[d]=new Array(_),m,e);for(var b,w,C=0,B=0;C<y;++C)if(b=A[C]){for(C>=B&&(B=C+1);!(w=x[B])&&++B<y;);b._next=w||null}}return h=new r.b(h,u),h._enter=f,h._exit=p,h}},function(t,e,n){"use strict";e.a=function(t){return arguments.length?this.property("__data__",t):this.node().__data__}},function(t,e,n){"use strict";function i(t,e,i){var o=n.i(a.a)(t),r=o.CustomEvent;"function"==typeof r?r=new r(e,i):(r=o.document.createEvent("Event"),i?(r.initEvent(e,i.bubbles,i.cancelable),r.detail=i.detail):r.initEvent(e,!1,!1)),t.dispatchEvent(r)}function o(t,e){return function(){return i(this,t,e)}}function r(t,e){return function(){return i(this,t,e.apply(this,arguments))}}var a=n(12);e.a=function(t,e){return this.each(("function"==typeof e?r:o)(t,e))}},function(t,e,n){"use strict";e.a=function(t){for(var e=this._groups,n=0,i=e.length;n<i;++n)for(var o,r=e[n],a=0,s=r.length;a<s;++a)(o=r[a])&&t.call(o,o.__data__,a,r);return this}},function(t,e,n){"use strict";e.a=function(){return!this.node()}},function(t,e,n){"use strict";var i=n(19),o=n(0);e.a=function(){return new o.b(this._exit||this._groups.map(i.a),this._parents)}},function(t,e,n){"use strict";var i=n(0),o=n(16);e.a=function(t){"function"!=typeof t&&(t=n.i(o.a)(t));for(var e=this._groups,r=e.length,a=new Array(r),s=0;s<r;++s)for(var u,l=e[s],c=l.length,h=a[s]=[],f=0;f<c;++f)(u=l[f])&&t.call(u,u.__data__,f,l)&&h.push(u);return new i.b(a,this._parents)}},function(t,e,n){"use strict";function i(){this.innerHTML=""}function o(t){return function(){this.innerHTML=t}}function r(t){return function(){var e=t.apply(this,arguments);this.innerHTML=null==e?"":e}}e.a=function(t){return arguments.length?this.each(null==t?i:("function"==typeof t?r:o)(t)):this.node().innerHTML}},function(t,e,n){"use strict";function i(){return null}var o=n(4),r=n(10);e.a=function(t,e){var a="function"==typeof t?t:n.i(o.a)(t),s=null==e?i:"function"==typeof e?e:n.i(r.a)(e);return this.select(function(){return this.insertBefore(a.apply(this,arguments),s.apply(this,arguments)||null)})}},function(t,e,n){"use strict";e.a=function(t,e,n){var i=this.enter(),o=this,r=this.exit();return i="function"==typeof t?t(i):i.append(t+""),null!=e&&(o=e(o)),null==n?r.remove():n(r),i&&o?i.merge(o).order():o}},function(t,e,n){"use strict";function i(){this.previousSibling&&this.parentNode.insertBefore(this,this.parentNode.firstChild)}e.a=function(){return this.each(i)}},function(t,e,n){"use strict";var i=n(0);e.a=function(t){for(var e=this._groups,n=t._groups,o=e.length,r=n.length,a=Math.min(o,r),s=new Array(o),u=0;u<a;++u)for(var l,c=e[u],h=n[u],f=c.length,p=s[u]=new Array(f),d=0;d<f;++d)(l=c[d]||h[d])&&(p[d]=l);for(;u<o;++u)s[u]=e[u];return new i.b(s,this._parents)}},function(t,e,n){"use strict";e.a=function(){for(var t=this._groups,e=0,n=t.length;e<n;++e)for(var i=t[e],o=0,r=i.length;o<r;++o){var a=i[o];if(a)return a}return null}},function(t,e,n){"use strict";e.a=function(){var t=new Array(this.size()),e=-1;return this.each(function(){t[++e]=this}),t}},function(t,e,n){"use strict";e.a=function(){for(var t=this._groups,e=-1,n=t.length;++e<n;)for(var i,o=t[e],r=o.length-1,a=o[r];--r>=0;)(i=o[r])&&(a&&4^i.compareDocumentPosition(a)&&a.parentNode.insertBefore(i,a),a=i);return this}},function(t,e,n){"use strict";function i(t){return function(){delete this[t]}}function o(t,e){return function(){this[t]=e}}function r(t,e){return function(){var n=e.apply(this,arguments);null==n?delete this[t]:this[t]=n}}e.a=function(t,e){return arguments.length>1?this.each((null==e?i:"function"==typeof e?r:o)(t,e)):this.node()[t]}},function(t,e,n){"use strict";function i(){this.nextSibling&&this.parentNode.appendChild(this)}e.a=function(){return this.each(i)}},function(t,e,n){"use strict";function i(){var t=this.parentNode;t&&t.removeChild(this)}e.a=function(){return this.each(i)}},function(t,e,n){"use strict";var i=n(0),o=n(10);e.a=function(t){"function"!=typeof t&&(t=n.i(o.a)(t));for(var e=this._groups,r=e.length,a=new Array(r),s=0;s<r;++s)for(var u,l,c=e[s],h=c.length,f=a[s]=new Array(h),p=0;p<h;++p)(u=c[p])&&(l=t.call(u,u.__data__,p,c))&&("__data__"in u&&(l.__data__=u.__data__),f[p]=l);return new i.b(a,this._parents)}},function(t,e,n){"use strict";var i=n(0),o=n(21);e.a=function(t){"function"!=typeof t&&(t=n.i(o.a)(t));for(var e=this._groups,r=e.length,a=[],s=[],u=0;u<r;++u)for(var l,c=e[u],h=c.length,f=0;f<h;++f)(l=c[f])&&(a.push(t.call(l,l.__data__,f,c)),s.push(l));return new i.b(a,s)}},function(t,e,n){"use strict";e.a=function(){var t=0;return this.each(function(){++t}),t}},function(t,e,n){"use strict";function i(t,e){return t<e?-1:t>e?1:t>=e?0:NaN}var o=n(0);e.a=function(t){function e(e,n){return e&&n?t(e.__data__,n.__data__):!e-!n}t||(t=i);for(var n=this._groups,r=n.length,a=new Array(r),s=0;s<r;++s){for(var u,l=n[s],c=l.length,h=a[s]=new Array(c),f=0;f<c;++f)(u=l[f])&&(h[f]=u);h.sort(e)}return new o.b(a,this._parents).order()}},function(t,e,n){"use strict";function i(){this.textContent=""}function o(t){return function(){this.textContent=t}}function r(t){return function(){var e=t.apply(this,arguments);this.textContent=null==e?"":e}}e.a=function(t){return arguments.length?this.each(null==t?i:("function"==typeof t?r:o)(t)):this.node().textContent}},function(t,e,n){"use strict";var i=n(11),o=n(5);e.a=function(t,e,r){arguments.length<3&&(r=e,e=n.i(i.a)().changedTouches);for(var a,s=0,u=e?e.length:0;s<u;++s)if((a=e[s]).identifier===r)return n.i(o.a)(t,a);return null}},function(t,e,n){"use strict";var i=n(11),o=n(5);e.a=function(t,e){null==e&&(e=n.i(i.a)().touches);for(var r=0,a=e?e.length:0,s=new Array(a);r<a;++r)s[r]=n.i(o.a)(t,e[r]);return s}},function(t,e,n){var i=n(35);"string"==typeof i&&(i=[[t.i,i,""]]);n(6)(i,{});i.locals&&(t.exports=i.locals)},function(t,e,n){var i=n(37);"string"==typeof i&&(i=[[t.i,i,""]]);n(6)(i,{});i.locals&&(t.exports=i.locals)},function(t,e,n){var i=n(38);"string"==typeof i&&(i=[[t.i,i,""]]);n(6)(i,{});i.locals&&(t.exports=i.locals)}])});

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : {}
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(20);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(4)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./context-menu.scss", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./context-menu.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(21);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(4)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./loading.scss", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./loading.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(22);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(4)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./modal.scss", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./modal.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(23);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(4)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./notification.scss", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./notification.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(24);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(4)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./popup.scss", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./popup.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(26);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(4)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./toast.scss", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./toast.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(27);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(4)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./tool-actions.scss", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./tool-actions.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ })
/******/ ]);
});
//# sourceMappingURL=gutter-measuring-tool.js.map